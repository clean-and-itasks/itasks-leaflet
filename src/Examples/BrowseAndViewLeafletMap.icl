module Examples.BrowseAndViewLeafletMap

import iTasks
import iTasks.Leaflet

Start w = doTasks browseAndViewLeafletMap w

browseAndViewLeafletMap :: Task LeafletMapWithoutState
browseAndViewLeafletMap
	= withShared defaultValue // create shared default value for the map
		(\smap -> 	(Hint "Browse Map" @>> updateSharedInformation [] smap) // update it here
					-||
					(Hint "View Browsing Map" @>> viewSharedInformation [] smap) ) // while viewing it here
	>>! \result -> Hint "Resulting map looks as follows" @>> viewInformation [] result // show final result
