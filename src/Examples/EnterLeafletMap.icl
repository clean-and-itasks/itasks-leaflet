module Examples.EnterLeafletMap

import iTasks
import iTasks.Leaflet

Start :: *World -> *World
Start world = doTasks enterLeafletMap world

enterLeafletMap :: Task LeafletMapWithoutState
enterLeafletMap
	=   Hint "Enter a Leaflet map:" @>> enterInformation []
	>>! \result -> Hint "You entered:" @>> viewInformation [ViewAs (gText{|*|} AsMultiLine o ?Just)] result
