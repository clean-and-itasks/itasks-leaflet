module Examples.ModifyLeafletMap

import Data.Func
import Data.GenHash
import qualified Data.Map as Map
import iTasks
import iTasks.Leaflet
import iTasks.LeafletNavalIcons
import StdEnv
import Text.HTML

Start w = doTasks modifyLeafletMap w

modifyLeafletMap :: Task ()
modifyLeafletMap = withShared ({LeafletMap|defaultMap & icons = defaultMap.icons ++ shipIcons, state = defaultValue})
	(\m =
		((allTasks [managePerspective m, manageState m, manageMapObjects m]) <<@ ScrollContent)
		-&&-
		manipulateMap m
	) <<@ ArrangeWithSideBar 0 LeftSide True @! ()
where
	defaultMap :: LeafletMap LeafletSimpleState
	defaultMap = defaultValue

manipulateMap :: !(Shared sds (LeafletMap LeafletSimpleState)) -> Task () | RWShared sds
manipulateMap m =
	updateSharedInformation
		[
			UpdateSharedUsing
				id
				(\read editorWrite = editorWrite)
				(	mapEditorWrite
					ValidEditor
					( customLeafletEditor defaultMapOptions eventHandlers defaultValue)
				)
		]
		m
	<<@ ApplyLayout (setUIAttributes (sizeAttr FlexSize FlexSize)) @! ()
where
	eventHandlers = simpleStateEventHandlers ++ [OnHtmlEvent onHtmlEvent]

	onHtmlEvent ::
		!String !(LeafletMap LeafletSimpleState)
		-> LeafletMap LeafletSimpleState
	onHtmlEvent "closewindows" l=:{objects} =
		{l & objects = 'Map'.filterWithKey (\_ obj = not (obj =: (Window _))) objects}
	onHtmlEvent _ leafletMap = leafletMap

managePerspective :: !(Shared sds (LeafletMap LeafletSimpleState)) -> Task () | RWShared sds
managePerspective m = Title "Perspective" @>> updateSharedInformation  []
	(mapReadWrite
		(\leafletMap = leafletMap.LeafletMap.perspective, \p leafletMap = ?Just {leafletMap & perspective = p}) ?None m
	) @! ()

:: ReadOnlyState =
	{ bounds :: !?LeafletBounds
	, center :: !?LeafletLatLng
	, zoom   :: !?Int
	}

derive class iTask ReadOnlyState

manageState :: !(Shared sds (LeafletMap LeafletSimpleState)) -> Task () | RWShared sds
manageState m
	= Title "State" @>>
	( 	viewSharedInformation [] (mapRead readOnlyState m)
	-&&-
		updateSharedInformation []
			(mapReadWrite (\{LeafletMap|state} = state, \sn leafletMap = ?Just {LeafletMap|leafletMap & state = sn})
				?None m)
	) @! ()
where
	readOnlyState :: !(LeafletMap LeafletSimpleState) -> ReadOnlyState
	readOnlyState m =
		{ ReadOnlyState
		| bounds = m.LeafletMap.bounds
		, center = m.LeafletMap.center
		, zoom   = m.LeafletMap.zoom
		}

// objects can currently only be viewed, as the editor for `HtmlTag` only works in view mode
manageMapObjects :: !(Shared sds (LeafletMap LeafletSimpleState)) -> Task () | RWShared sds
manageMapObjects m =
	Title "View objects" @>> viewSharedInformation [ViewAs toPrj, ViewUsing id $ leafletEditorGeneric ViewValue] m -|| addDemoObjects m @! ()
where
	toPrj :: !(LeafletMap LeafletSimpleState) -> [LeafletObject LeafletSimpleState]
	toPrj m = 'Map'.elems m.LeafletMap.objects

	addDemoObjects ::
		 !(Shared sds (LeafletMap LeafletSimpleState))
		 -> Task (Task (LeafletMap LeafletSimpleState)) | RWShared sds
	addDemoObjects m
		=  Hint "Add objects:" @>> enterChoiceAs [ChooseFromCheckGroup fst] options snd
		>^* [OnAction (Action "Add") (hasValue id)]
	where
	 	options =
			[("Random marker",addRandomMarker m)
			,("Marker at cursor position",addMarkerAtCursor m)
			,("Line connecting current markers",addMarkerConnectingLine m)
			,("Polygon from current markers",addMarkerConnectingPolygon m)
			,("Circle at cursor position",addCircleAtCursor m)
			,("Rectangle around current perspective",addRectangleAroundCurrentPerspective m)
			,("Some window",addWindow m)
			]

	addRandomMarker ::
		 !(Shared sds (LeafletMap LeafletSimpleState))
		 -> Task (LeafletMap LeafletSimpleState) | RWShared sds
	addRandomMarker m =
		get randomInt -&&- get randomInt @ toRandomMarker
		>>- \(markerId, marker) =
		upd (\(l=:{LeafletMap|objects}) = ({LeafletMap|l & objects = 'Map'.put markerId marker objects})) m

	toRandomMarker :: !(!Int,!Int) -> (!LeafletObjectID, !LeafletObject LeafletSimpleState)
	toRandomMarker (rLat,rLng)
		= 	( LeafletObjectID markerId
			, Marker
				{ position = {LeafletLatLng|lat = lat, lng = lng}
				, rotation = ?None
				, tint = ?None
				, title = \_ = ?Just $ Text markerId
				, icon = ?Just icon
				, popup = ?None
				}
			)
	where
		lat = 52.0 + (toReal (500 + (rLat rem 1000)) / 1000.0)
		lng = 6.0 + (toReal (500 + (rLng rem 1000)) / 1000.0)
		markerId = "RANDOM-" <+++ rLat <+++ rLng
		icon = shipIconId (?Just (rLat rem 360)) OrangeShip False

	addMarkerConnectingLine ::
		 !(Shared sds (LeafletMap LeafletSimpleState))
		 -> Task (LeafletMap LeafletSimpleState) | RWShared sds
	addMarkerConnectingLine m = upd (\l=:{LeafletMap|objects} = {LeafletMap|l & objects = addLineTo objects}) m
	where
		addLineTo ::
			!(Map LeafletObjectID (LeafletObject LeafletSimpleState))
			-> Map LeafletObjectID (LeafletObject LeafletSimpleState)
		addLineTo objects =
			'Map'.put
				(LeafletObjectID "markerConnection")
				(Polyline
					{LeafletPolyline
					| style = [Style (LineStrokeColor "#f0f"), Style (LineStrokeWidth 4)]
					, points     = points objects
					, editable   = True
					}
				)
				objects
		points objects = [position \\ (_, Marker {LeafletMarker|position}) <- 'Map'.toList objects]

	addMarkerConnectingPolygon ::
		 !(Shared sds (LeafletMap LeafletSimpleState))
		 -> Task (LeafletMap LeafletSimpleState) | RWShared sds
	addMarkerConnectingPolygon m = upd (\l=:{LeafletMap|objects} = {LeafletMap|l & objects = addPolygonTo objects}) m
	where
		addPolygonTo ::
			!(Map LeafletObjectID (LeafletObject LeafletSimpleState))
			-> Map LeafletObjectID (LeafletObject LeafletSimpleState)
		addPolygonTo objects =
			'Map'.put
				(LeafletObjectID "markerConnection")
				(Polygon
					{LeafletPolygon |
						style =
							[ Style (AreaLineStrokeColor "#000") , Style (AreaLineStrokeWidth 2) , Style (AreaFillColor "#0f0") ]
					, points = points objects
					, editable = True
					}
				) objects
		points objects = [position \\ (_, Marker {LeafletMarker|position}) <- 'Map'.toList objects]

	addMarkerAtCursor ::
		 !(Shared sds (LeafletMap LeafletSimpleState))
		 -> Task (LeafletMap LeafletSimpleState) | RWShared sds
	addMarkerAtCursor m
		= upd
			(\(l=:{LeafletMap|objects, state = {cursor}}) =
				{LeafletMap|l & objects = maybe objects (withMarkerFromCursor objects) cursor }
			) m
	where
		withMarkerFromCursor ::
			!(Map LeafletObjectID (LeafletObject LeafletSimpleState)) !LeafletLatLng
			-> Map LeafletObjectID (LeafletObject LeafletSimpleState)
		withMarkerFromCursor objects position =
			'Map'.put
				(LeafletObjectID "CURSOR")
				(Marker
					{ position = position
					, rotation = ?None
					, tint = ?None
					, title = \_ = ?None
					, icon = ?Just (shipIconId (?Just 0) OrangeShip False)
					, popup = ?None
					})
				objects

	addCircleAtCursor ::
		 !(Shared sds (LeafletMap LeafletSimpleState))
		 -> Task (LeafletMap LeafletSimpleState) | RWShared sds
	addCircleAtCursor m
		= upd
			(\(l=:{LeafletMap|objects, state = {cursor}}) =
				{LeafletMap|l & objects = withCircleFromCursor cursor objects}) m
	where
		withCircleFromCursor ::
			!(?LeafletLatLng) !(Map LeafletObjectID (LeafletObject LeafletSimpleState))
			-> Map LeafletObjectID (LeafletObject LeafletSimpleState)
		withCircleFromCursor ?None objects = objects
		withCircleFromCursor (?Just position) objects =
			'Map'.put
				(LeafletObjectID "CIRCLE_CURSOR")
				(Circle {center = position , radius = 100000.0, editable = True, style = []})
				objects

	addRectangleAroundCurrentPerspective ::
		 !(Shared sds (LeafletMap LeafletSimpleState))
		 -> Task (LeafletMap LeafletSimpleState) | RWShared sds
	addRectangleAroundCurrentPerspective m
		= upd
			(\l=:{LeafletMap|bounds,objects} =
				{LeafletMap|l & objects = maybe objects (withRectangleAroundCurrentPerspective objects)  bounds}
			) m
	where
		withRectangleAroundCurrentPerspective objects bounds =
			'Map'.put
				(LeafletObjectID "RECT_PERSPECTIVE")
				(Rectangle {LeafletRectangle|bounds = bounds, editable = True, style = []})
				objects

	addWindow ::
		!(Shared sds (LeafletMap LeafletSimpleState))
		-> Task (LeafletMap LeafletSimpleState) | RWShared sds
	addWindow m
		= upd
			(\l=:{LeafletMap|objects} =
				{LeafletMap| l & objects = 'Map'.put (LeafletObjectID "WINDOW") (Window window) objects}
			) m
	where
		window =
			{ initPosition   = ?Just {x = 100, y = 100}
			, title          = "Test Window"
			, content        = DivTag []
				[H1Tag [] [Text "This is test content!"]
				,ATag [HrefAttr "#",OnclickAttr "itasks.htmlEvent(event, 'closewindows')"] [Text "Close windows"]
				]
			, relatedMarkers = [(LeafletObjectID "home", [])]
			}

derive gDefault LeafletSimpleState

/**
 * The type synonym is used as a "trick" to make the type of the state of the leaflet map known to the compiler.
 * As we may not define `gEditor` for `LeafletMap` generically
 * as the definition requires to have an instance of the `TC` for the type as it uses dynamics,
 * and not all types implement this class.
 * It is not possible to define gEditor{|LeafletMap LeafletSimpleState|} either.
 */
:: LeafletMapWithSimpleState :== LeafletMap LeafletSimpleState
gEditor{|LeafletMapWithSimpleState|} purpose = leafletEditorGeneric purpose
