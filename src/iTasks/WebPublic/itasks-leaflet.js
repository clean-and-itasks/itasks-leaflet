/**
 * Formats the provided geo coordinates to a string in degrees/minutes/seconds notation.
 *
 * @param {number} lng The longitude of the geo position.
 * @param {number} lat The latitude of the geo position.
 * @returns The formatted string.
 */
L.dmsFormatter = function(lng, lat) {
	const dmsString = function(deg) {
		let d = Math.floor(Math.abs(deg));
		const minfloat = (Math.abs(deg) - d) * 60;
		let m = Math.floor(minfloat);
		const secfloat = (minfloat - m) * 60;
		let s = secfloat.toFixed(2);
		if (s == 60) {
			m++;
			s = "00";
		}
		if (m == 60) {
			d++;
			m = "00";
		}
		if (s < 10) {
			s = "0" + s;
		}
		if (m < 10) {
			m = "0" + m;
		}
		return ("" + d + "&deg; " + m + "' " + s + "''");
	};

	const NorS = (lat > 0) ? 'N' : 'S';
	const EorW = (lng > 0) ? 'E' : 'W';
	return dmsString(lat) + " " + NorS + " " + dmsString(lng) + " " + EorW;
};

button_url = "";
label = "";

L.Control.Legend =  L.Control.extend({

	options: {
	  position: 'topleft'
	},

	onAdd: function (map) {
	  container = L.DomUtil.create('div', 'leaflet-bar');
	  legendButton = L.DomUtil.create('a',"leaflet-interactive polyline-measure-unicode-icon");
	  container.appendChild(legendButton);
	  legendButton.innerHTML = label;
	  legendButton.title = "View legend"

	  legendButton.onclick = function(){
		window.open(button_url, "_blank");
	  }

	  return container;
	}
  });

L.control.legend = function (options) {
	button_url = options.url;
	label = options.label;
	return new L.Control.Legend(options);
};

/**
 * This part of the file implements a significant portion of the otherwise Clean implementation of the leaflet editor.
 * The purpose of this separation is to prevent the many context switches that otherwise take place.
 *
 * Two main functions related to the leaflet editor are part of this file.
 * The first is leafletCreateMarkers, a function called when new markers are received from the server.
 * The second is the leafletPixiDraw function, called when the PixiOverlay layer is drawn.
 */
function leafletMarkerOnMouseOver(me, markerId, taskId, editorId, e) {
	// Changes the cursor from grab to pointer, this is reverted by `leafletMarkerOnMouseOut`.
	Array.from(document.getElementsByClassName('leaflet-pixi-overlay')).map(elem => elem.style.cursor = "pointer");
	// Delay sending the event (can be cancelled in `leafletMarkerOnMouseOut`) to prevent server from being overloaded
	// with events if the user moves the mouse over a large number of objects.
	me.onMouseOverTimeout =
		setTimeout(
			() => {delete me.onMouseOverTimeout; me.doEditEvent(taskId, editorId, [["LDMarkerHover", markerId]]);}, 200
		);
	me.markerCoordinates[markerId] = [e.data.originalEvent.clientX, e.data.originalEvent.clientY];
}

function addTooltip (me, markerId, title) {
	if (me.markerCoordinates == undefined) {return;}
	const coordinates = me.markerCoordinates[markerId];
	if (coordinates == null) {return ;}
	const x = coordinates[0] + 10;
	const y = coordinates[1] + 10;

	const div = document.createElement("div");

	div.className  = "marker-tooltip";
	div._markerId  = markerId;
	div.innerHTML  = title;
	div.style.left = x.toString() + "px";
	div.style.top  = y.toString() + "px";

	document.body.appendChild(div);
}

function removeTooltip (markerId) {
	let tooltips = document.body.getElementsByClassName("marker-tooltip");
	for (var i = tooltips.length - 1; i >= 0; i--) {
		if (tooltips[i]._markerId == markerId) {
			document.body.removeChild(tooltips[i]);
		}
	}
}

function leafletMarkerOnMouseOut(me, markerId, taskId, editorId, e) {
	// Changes the cursor from pointer to grab , this event is precided by `leafletMarkerOnMouseOver`.
	Array.from(document.getElementsByClassName('leaflet-pixi-overlay')).map(elem => elem.style.cursor = "grab");

	if (me.onMouseOverTimeout) {
		clearTimeout(me.onMouseOverTimeout);
		delete me.onMouseOverTimeout;
	} else {
		me.doEditEvent(taskId, editorId, [["LDCancelMarkerHover", markerId]]);
	}
	removeAllTooltips(me);
}

function leafletCreateTitles(me, markerIdsWithTitle) {
	markerIdsWithTitle.forEach
		(markerIdWithTitle =>
			{
				const markerId = markerIdWithTitle[0];
				const title = markerIdWithTitle[1];
				if (title != null && title != undefined) {
					addTooltip(me, markerId, title);
				} else {
					removeTooltip(markerId);
				}
			}
		)
}

function leafletCreateMarker(me, map, markerWithId) {
	const marker = markerWithId[1];
	marker.markerId = markerWithId[0];

	const texture = me.textures[marker.icon];
	const sprite = new PIXI.Sprite(texture.texture);
	sprite.hitArea = texture.hitArea;
	const coords = map.project(marker.position);

	// Set all sprite properties
	sprite.markerId =    marker.markerId;
	sprite.angle =       (marker.rotation ? marker.rotation : 0);
	// `?None` tint is encoded as null.
	sprite.tint =        marker.tint ?? 0xFFFFFF;
	sprite.interactive = true;
	sprite.lat =         marker.position.lat;
	sprite.lng =         marker.position.lng;
	sprite.x =           coords.x;
	sprite.y =           coords.y;

	taskId = me.attributes.taskId;
	editorId = me.attributes.editorId;
	sprite.mouseover = (e) => leafletMarkerOnMouseOver (me, marker.markerId, taskId, editorId, e);
	sprite.mouseout = (e) => leafletMarkerOnMouseOut (me, marker.markerId, taskId, editorId, e);

	// Rotate the sprite around its center
	sprite.anchor.set(0.5,0.5);
	// Set the scale to the current scale
	sprite.scale.set(me.scale);

	if (marker.popup) {
		// Popups closed by the user have to be synced with the server.
		const onPopupRemove = () => me.doEditEvent(taskId, editorId, [["LDClosePopup", marker.markerId]]);
		const popup =
			// Do not move map to make new popups visible, as this can cause crashes in rare timing circumstances.
			L.popup({closeOnClick: false, autoPan: false})
				.setLatLng([sprite.lat, sprite.lng])
				.setContent(marker.popup)
				.on('remove', onPopupRemove);
			map.addLayer(popup);
		// If the corresponding sprite is removed, no event should be sent to the server.
		sprite.on('removed', () => {popup.off('remove', onPopupRemove); map.removeLayer(popup)});
	}

	me.container.addChild(sprite);

	const windows = me.windows.get(marker.markerId);
	if(!windows)
		return;
	windows.forEach(function(window) {window.onMarkerAdd(sprite)});
}

function removeAllTooltips(me) {
	me.markerCoordinates = {};
	let tooltips = document.body.getElementsByClassName("marker-tooltip");
	for (var i = tooltips.length - 1; i >= 0; i--) {
		document.body.removeChild(tooltips[i]);
	}
}

function leafletCreateMarkersCb(me) {
	const markers = me.attributes.markers[1];

	// Fetch here once to pass to the leafletCreateMarker function. Prevents constant fetching of the value
	const map = me.map;
	// Lose reference to the current array of children
	me.container.removeChildren();
	removeAllTooltips(me);
	// Markers are arrays with two elements, the first is a string "Marker"
	// , which was needed in the Clean code, but not here.
	markers.forEach(m => leafletCreateMarker(me, map, m));
	me.attributes.markers[1] = [];

	// After creating all new values, force a redraw
	me.overlay.redraw();
}

function leafletCreateMarkers(me) {
	// Drawing the markers is delayed while the user is zooming or moving the map.
	if (me.delayDrawMarkerMove || me.delayDrawMarkerZoom) {
		me.delayDrawMarkerCb = () => leafletCreateMarkersCb(me);
	} else {
		leafletCreateMarkersCb(me);
	}
}

function leafletPixiDraw(me, utils) {
	const container = utils.getContainer();
	const renderer = utils.getRenderer();
	const project = utils.latLngToLayerPoint;
	const scale = 1 / utils.getScale();
	const sprites = me.container.children;

	if (me.interactionManager == null) {
		me.interactionManager = renderer.plugins.interaction;
		me.interactionManager.cursorStyles = { default: "inherit", pointer: "pointer", grab: "grab"};
	}

	sprites.forEach(function(sprite) {
		const coords = project([sprite.lat, sprite.lng]);
		sprite.x = coords.x;
		sprite.y = coords.y;
		if (me.scale != scale) {
			sprite.scale.set(scale);
		}
	});

	me.scale = scale;
	renderer.render(container);
}

/**
 * The purpose of having a single JS function creating all polylines is to prevent the many context switches that would
 * take place if calling a JS function for each polyline.
 */
function leafletCreatePolylines(me) {
	const map = me.map;

	// Remove current polylines.
	if (Array.isArray(me.currentPolylines)) {
		me.currentPolylines.forEach(p => map.removeLayer(p));
	}
	me.currentPolylines = [];

	// Add new polylines.
	const polylines = me.attributes.polylines;
	polylines.forEach(p => leafletCreatePolyline(me, map, p));
	me.attributes.polylines = [];
}

function applyStyle(style, options) {
	const styleType = style[0];
	const styleDef = style[1];

	switch (styleType) {
		case "Style":
			const directStyleType = styleDef[0];
			const directStyleVal = styleDef[1];
			switch(directStyleType) {
				case "LineStrokeColor":
					options.color = directStyleVal;
					break;
				case "LineStrokeWidth":
					options.weight = directStyleVal;
					break;
				case "LineOpacity":
					options.opacity = directStyleVal;
					break;
				case "LineDashArray":
					options.dashArray = directStyleVal;
					break;
				default:
					console.error("Unknown direct style: ", directStyleType);
			}
			break;
		case "Class":
			options.className = styleDef;
			break;
		default:
			console.error("Unknown style type: ", styleType);
	}
}

/**
 * This only works for non-editable polylines. For editable ones the Clean function `createPolyline` has
 * to be used.
 */
function leafletCreatePolyline(me, map, p) {
	var options = {};
	p.attributes.style.forEach(style => applyStyle(style, options));
	const polyline = L.polyline(p.attributes.points, options);
	me.currentPolylines.push(polyline);
	polyline.addTo(me.map);
}

L.Window = L.Control.extend({
	initialize: function (baseLayers, overlays, options) {
		this._relatedMarkers = {};
	},
	options: {
		position: 'topleft'
	},
	setInitPos: function(pos) {
		this._initPos = pos;
	},
	setTitle: function(title) {
		if (this._titleEl) {
			this._titleEl.innerHTML = title;
		} else {
			this._initTitle = title;
		}
	},
	setContent: function(content) {
		if (this._contentNode) {
			morphdom(this._contentNode.firstChild, content, {
				onBeforeElUpdated: this.morphDomForWindowElements
			});
		} else {
			this._initContent = content;
		}
	},
	setRelatedMarkers: function(me, markers) {
		// remove all markers
		this.removeFromMap(me.windows);
		for (markerId in this._relatedMarkers) {
			if (markerId in this._relatedMarkerConnectors) {
				this._relatedMarkerConnectors[markerId].polyline.remove();
			}
		};
		this._relatedMarkers = {};
		this._relatedMarkerConnectors = {};
		markers.forEach((marker) => this.addRelatedMarker(marker));
		// this actually creates the connectors
		me.container.children.forEach((marker) => this.onMarkerAdd(marker));
	},
	addRelatedMarker: function(marker) {
		const markerId   = marker[0];
		const lineStyles = marker[1];
		var options = {};

		lineStyles.forEach((lineStyle) => {
			const lineStyleConstr    = lineStyle[0];
			const lineStyleConstrArg = lineStyle[1];

			switch (lineStyleConstr) {
				case "Style":
					const lineStyleAttr    = lineStyleConstrArg[0];
					const lineStyleAttrVal = lineStyleConstrArg[1];

					switch (lineStyleAttr) {
						case "LineStrokeColor":
							options.color     = lineStyleAttrVal;
							break;
						case "LineStrokeWidth":
							options.weight    = lineStyleAttrVal;
							break;
						case "LineOpacity":
							options.opacity   = lineStyleAttrVal;
							break;
						case "LineDashArray":
							options.dashArray = lineStyleAttrVal;
							break;
						default:
							throw new Error("Unknown line style attribute: " + lineStyleAttr);
					}
					break;
				case "Class":
					options.className = lineStyleConstrArg;
					break;
				default:
					throw new Error("Unknown line style constructor: " + lineStyleConstr);
			}
		});

		this._relatedMarkers[markerId] = options;
	},
	onAdd: function (map) {
		this._map = map;
		this._collapsed = false;
		this.ARROW_UP = '\u23F6';
		this.ARROW_DOWN = '\u23F7';
		this._fullHeight = 0;

		const container = this._container = L.DomUtil.create('div', 'itasks-leaflet-window');

		// add title bar
		const titleBar = L.DomUtil.create('div', 'titlebar', container);

		const titleSpan = L.DomUtil.create('div', 'titleText', titleBar);
		titleSpan.innerHTML = this._initTitle;
		delete this._initTitle;
		this._titleEl = titleSpan;

		const collapseButton = this._collapseButton = L.DomUtil.create('a', 'button', titleBar);
		collapseButton.innerHTML = this.ARROW_UP;
		collapseButton.style.cursor = "pointer";
		L.DomEvent.on(collapseButton, 'mouseup', this._onCollapseButtonClick, this);

		const closeButton = this._closeButton = L.DomUtil.create('a', 'button', titleBar);
		closeButton.innerHTML = 'x';
		closeButton.style.cursor = "pointer";
		L.DomEvent.on(closeButton, 'mouseup', this._onCloseButtonClick, this);

		// add content container
		this._contentNode = L.DomUtil.create('div', '', container);
		this._contentNode.innerHTML = this._initContent;
		delete this._initContent;

		// absolute -> otherwise windows influence each other if multiple are present
		container.style = "margin: 0px; position: absolute;";
		this._setPos(this._initPos);

		L.DomEvent.disableClickPropagation(container);
		L.DomEvent.disableScrollPropagation(container);
		L.DomEvent.on(container, 'contextmenu', L.DomEvent.stopPropagation);
		this.dragging = false;
		L.DomEvent.on(titleBar, 'mousedown', function(e) {
			// store current size of map container & title bar
			// required to prevent title var from being dragged out of view
			const mapContainerSize   = this._map.getSize();
			this._mapContainerWidth  = mapContainerSize.x;
			this._mapContainerHeight = mapContainerSize.y;
			this._titleBarWidth      = titleBar.offsetWidth;
			this._titleBarHeight     = titleBar.offsetHeight;

			// store delta between left upper corner of window and mouse position
			const containerRect = this._container.getBoundingClientRect();
			this.dragging = [e.clientX - containerRect.left, e.clientY - containerRect.top];
			L.DomUtil.disableTextSelection();
			this._container.style.opacity = 0.6;
			L.DomUtil.toFront(container);
		}, this);
		L.DomEvent.on(document, 'mouseup', this._mouseUp, this);
		L.DomEvent.on(document, 'mousemove', this._mouseMove, this);

		this._relatedMarkerConnectors = {};
		map.on('zoom', this._updateRelatedMarkerConnectorPositions, this);
		map.on('move', this._updateRelatedMarkerConnectorPositions, this);

		return container;
	},
	addTo: function(map) {
		L.Control.prototype.addTo.call(this, map);
	},
	onCreate: function(markers) {
		// handle related markers already added.
		// this is done after adding the window,
		// as we need the content's size
		// to position the connectors properly
		// This function is called manually in the createWindow function
		markers.forEach((m) => this.onMarkerAdd(m));
	},
	// This function is called whenever a new marker is created
	onMarkerAdd: function(marker) {
		if (marker.markerId in this._relatedMarkers) {
			this._addRelatedMarker(marker, this._relatedMarkers[marker.markerId]);
		}
	},
	onMarkerRemove: function(marker) {
		const markerId = marker.markerId;
		if (markerId in this._relatedMarkers) {
			this._relatedMarkerConnectors[markerId].polyline.remove();
			delete this._relatedMarkerConnectors[markerId];
		}
	},
	_addRelatedMarker: function(marker, options) {
		const connector = {polyline: L.polyline([], options), position: [marker.lat, marker.lng]};
		if (this._relatedMarkerConnectors[marker.markerId]) {
			this._relatedMarkerConnectors[marker.markerId].polyline.remove();
		}
		this._relatedMarkerConnectors[marker.markerId] = connector;
		connector.polyline.addTo(this._map);
		this._updateRelatedMarkerConnectorPosition(connector);
	},
	_updateRelatedMarkerConnectorPositions: function() {
		Object.values(this._relatedMarkerConnectors).forEach(this._updateRelatedMarkerConnectorPosition, this);
	},
	_updateRelatedMarkerConnectorPosition: function(connector) {
		const windowCentrePos = { x: this._position.x + this._container.offsetWidth/2
								, y: this._position.y + this._container.offsetHeight/2 };
		connector.polyline.setLatLngs([this._map.containerPointToLatLng(windowCentrePos), connector.position]);
	},
	_mouseUp: function(e) {
		this.dragging = false;
		L.DomUtil.enableTextSelection();
		this._container.style.opacity = 1.0;
	},
	_mouseMove: function(e) {
		const dragging = this.dragging;
		if (dragging) {
			const mapPos = this._map.mouseEventToContainerPoint(e);
			// delta (stored in 'dragging') to compensate for where inside title bar drag was started
			// restrict position such that title bar is never dragged outside of map container
			const x = Math.min(this._mapContainerWidth  - this._titleBarWidth,  Math.max(0, mapPos.x - dragging[0]));
			const y = Math.min(this._mapContainerHeight - this._titleBarHeight, Math.max(0, mapPos.y - dragging[1]));
			this._setPos({x: x, y: y});
			this._updateRelatedMarkerConnectorPositions();
		}
	},
	_onCloseButtonClick: function () {
		this._onWindowClose(); // injected by Clean code (sends remove event to server)
		this.remove();
	},
	_onCollapseButtonClick: function () {
		this._fullHeight = this._collapsed ? this._fullHeight : this._contentNode.offsetHeight;
		this._collapsed = !this._collapsed;
		this._contentNode.style.visibility = this._collapsed ? "collapse" : "visible";
		this._contentNode.style.height = this._collapsed ? "0px" : this._fullHeight + "px";
		this._collapseButton.innerHTML = this._collapsed ? this.ARROW_DOWN : this.ARROW_UP;
		this._updateRelatedMarkerConnectorPositions();
	},
	_setPos: function(p) {
		this._position = p;
		this._container.style.transform = "translate(" + p.x + "px, " + p.y + "px)";
	},
	onRemove: function (map) {
		Object.values(this._relatedMarkerConnectors).forEach((m) => m.polyline.remove());
		L.DomEvent.off(document, 'mouseup', this._mouseUp, this);
		L.DomEvent.off(document, 'mousemove', this._mouseMove, this);
		map.off('zoom', this._updateRelatedMarkerConnectorPositions, this);
		map.off('move', this._updateRelatedMarkerConnectorPositions, this);
	},
	removeFromMap: function (map) {
		for (markerId in this._relatedMarkers) {
			this._removeFromMap(map, markerId);
		}
	},
	_removeFromMap: function (map, markerId) {
		set = map.get(markerId);
		if (set) {
			set.delete(this);
			if (set.size === 0) {
				map.delete(markerId);
			}
		}
	}
});
L.window = function (options, source) {
	return new L.Window(options, source);
};

function dummyCallback () {}
