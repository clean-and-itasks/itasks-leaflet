implementation module iTasks.Leaflet

import ABC.Interpreter.JavaScript
from Base64 import base64Encode
import Control.GenBimap
import qualified Data.Foldable as DF
from Data.Foldable import class Foldable
import Data.List => qualified group
import qualified Data.Map as Map
from Data.Map import derive gEq Map
import Data.Map.GenJSON
import Data.Maybe
import qualified Data.Set as Set
from Data.Set import instance == (Set k), instance Foldable Set, derive gEq Set
import Data.Set.GenJSON
import Gast => qualified label
import Gast.Gen
import StdEnv
import StdMisc, Data.Tuple, Data.Error, Data.Func, Text, Data.Functor
import Text.GenPrint, Text.HTML, Text.HTML.GenJSON
import iTasks
import iTasks.UI.Definition, iTasks.UI.Editor, iTasks.UI.Editor.Modifiers, iTasks.UI.JavaScript
from iTasks.UI.Editor.Common import diffChildren, :: ChildUpdate (..)
import iTasks.WF.Derives

LEAFLET_JS              :== "leaflet/leaflet.js"
LEAFLET_CSS             :== "leaflet/leaflet.css"
LEAFLET_JS_MORPHDOM     :== "leaflet/morphdom-umd.min.js" // required for "leaflet-window.js"
LEAFLET_JS_EDITABLE     :== "leaflet/Leaflet.Editable.js"
PIXI_OVERLAY_JS         :== "leaflet/L.PixiOverlay.min.js"
PIXI_JS                 :== "leaflet/pixi.min.js"
LEAFLET_MOUSEPOS_JS     :== "leaflet/L.Control.MousePosition.js"
LEAFLET_MOUSEPOS_CSS    :== "leaflet/L.Control.MousePosition.css"
LEAFLET_DISTMEASURE_JS  :== "leaflet/Leaflet.PolylineMeasure.js"
LEAFLET_DISTMEASURE_CSS :== "leaflet/Leaflet.PolylineMeasure.css"
ITASKS_LEAFLET_JS       :== "itasks-leaflet.js"
ITASKS_LEAFLET_CSS      :== "itasks-leaflet.css"

DEFAULT_WINDOW_POS   :== (50, 50)
MAX_WINDOW_OFFSET    :== 100

:: IconOptions =
	{ iconUrl   :: !String
	, iconSize  :: ![Int]
	}

derive JSONEncode IconOptions

derive gToJS LeafletLatLng

leafletPointsOf :: !(LeafletObject s) -> [LeafletLatLng]
leafletPointsOf (Marker m) = [m.position]
leafletPointsOf (Polyline l) = l.LeafletPolyline.points
leafletPointsOf (Polygon p) = p.LeafletPolygon.points
// Returns the top, bottom, left and right point of the circle to fit the bounds properly.
leafletPointsOf (Circle c) =
	[ {lat = c.LeafletCircle.center.lat + c.LeafletCircle.radius / oneDegreeInMeters, lng = c.LeafletCircle.center.lng}
	, {lat = c.LeafletCircle.center.lat - c.LeafletCircle.radius / oneDegreeInMeters, lng = c.LeafletCircle.center.lng}
	, {lat = c.LeafletCircle.center.lat, lng = c.LeafletCircle.center.lng + c.LeafletCircle.radius / oneDegreeInMeters}
	, {lat = c.LeafletCircle.center.lat, lng = c.LeafletCircle.center.lng - c.LeafletCircle.radius / oneDegreeInMeters}
	]
where
	// This is an approximation.
	oneDegreeInMeters = 111139.0
leafletPointsOf (Rectangle {LeafletRectangle | bounds=b}) = [b.southWest, b.northEast]
leafletPointsOf (Window w) = []

//* Accumulator record for storing the min/max lat/lngs for the leaflet objects of a leaflet map.
:: MinAndMaxBounds =
	{ minLat :: !Real
	, minLng :: !Real
	, maxLat :: !Real
	, maxLng :: !Real
	}

leafletBoundingRectangleOf :: !(Map LeafletObjectID (LeafletObject s)) -> LeafletBounds
leafletBoundingRectangleOf objects
	# mbBounds = 'Map'.foldlWithKey updateBoundsAcc ?None objects
	= case mbBounds of
		?Just {minLat, minLng, maxLat, maxLng} =
			{ southWest = {lat=minLat, lng=minLng}
			, northEast = {lat=maxLat, lng=maxLng}}
		?None = defaultValue
where
	updateBoundsAcc ::
		!(?MinAndMaxBounds) LeafletObjectID !(LeafletObject s) -> (?MinAndMaxBounds)
	updateBoundsAcc mbBoundsAcc _ object =
		foldl
			(\mbBoundsAcc position=:{lat, lng} =
				?Just $
					maybe {minLat = lat, minLng = lng, maxLat = lat, maxLng = lng} (updateBounds position) mbBoundsAcc
			) mbBoundsAcc $ leafletPointsOf object
	where
		updateBounds :: !LeafletLatLng !MinAndMaxBounds -> MinAndMaxBounds
		updateBounds {lat, lng} {minLat, minLng, maxLat, maxLng} =
			{minLat = min lat minLat , minLng = min lng minLng, maxLat = max lat maxLat , maxLng = max lng maxLng}

:: LeafletEdit
	= LDSetManualPerspective
	//Current state
	| LDSetZoom            !Int
	| LDSetCenter          !LeafletLatLng
	| LDSetBounds          !LeafletBounds
	//Updating windows
	| LDRemoveWindow       !LeafletObjectID
	| LDClosePopup         !LeafletObjectID
	| LDUpdateObject       !LeafletObjectID !LeafletObjectUpdate
	//Events
	| LDMapClick           !LeafletLatLng
	| LDMapDblClick        !LeafletLatLng
	| LDMarkerClick        !LeafletObjectID
	| LDHtmlEvent          !String
	| LDMarkerHover       !LeafletObjectID
	| LDCancelMarkerHover !LeafletObjectID

:: LeafletObjectUpdate
	= UpdatePolyline  ![LeafletLatLng]
	| UpdatePolygon   ![LeafletLatLng]
	| UpdateCircle    !LeafletLatLng !Real
	| UpdateRectangle !LeafletBounds

svgIconURL :: !SVGElt !(!Int,!Int) -> String
svgIconURL svgelt (width,height) = "data:image/svg+xml;base64," +++ base64Encode svg
where
    svg = concat ["<svg xmlns=\"http://www.w3.org/2000/svg\" width=\""
		, toString width, "\" height=\"", toString height, "\">", toString svgelt, "</svg>"]

openStreetMapTiles :: TileLayer
openStreetMapTiles =
	{ url         = "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
	, attribution = ?Just $ Html "&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a>"
	}

leafletEditor :: !Bool -> Editor LeafletMapWithoutState LeafletMapWithoutState
leafletEditor viewOnly =
	leafEditorToEditor $ leafEditorForLeafletMap {defaultMapOptions & viewOnly = viewOnly} (const id)

leafletEditorGeneric ::
	!EditorPurpose -> Editor (LeafletMap s) (EditorReport (LeafletMap s))
	| gEq{|*|}, gDefault{|*|}, JSONEncode{|*|}, TC s
leafletEditorGeneric purpose =
	mapEditorWrite ValidEditor $ leafEditorToEditor $
	leafEditorForLeafletMap {defaultMapOptions & viewOnly = purpose =: ViewValue} (const id)

/**
 * Leaflet editors do not send updates when processing `onEdit` events. All changes are reflected in a new value
 * which results in a `onRefresh` which takes care of sending updates to the editor. As multiple `onEdit` events can
 * occur before a `onRefresh`, we have to keep track of the current state on the server, which is updated by the
 * `onEdit` events, and the current state on the client which is used by `onRefresh` to determine the updates for the
 * client.
 */
:: InternalState s = {currentOnServer :: !LeafletMap s, currentOnClient :: !LeafletMap s}

leafEditorForLeafletMap ::
	!(MapOptions s) !(JSVal *JSWorld -> *JSWorld)
	-> LeafEditor [LeafletEdit] (InternalState s) (LeafletMap s) (LeafletMap s)
	| JSONEncode{|*|}, gDefault{|*|}, gEq{|*|} s
leafEditorForLeafletMap
	mapOptions=:
		{viewOnly, viewMousePosition, viewMeasureDistance, viewLegend, morphChangedWindowElements
		, sendMarkersToClientPred}
	postInitUI
	= JavaScriptInit initUI @>>
	{ LeafEditor
	| onReset        = onReset
	, onEdit         = onEdit
	, onRefresh      = onRefresh
	, writeValue     = writeValue
	}
where
	onReset ::
		!UIAttributes !(?(LeafletMap s)) !*VSt
		-> (!MaybeError String *(UI, (InternalState s), ?(LeafletMap s)), *VSt)
		| JSONEncode{|*|}, gDefault{|*|} s
	onReset attr mbval vst
		# val=:{LeafletMap|perspective,tilesUrls,objects,icons,state} = fromMaybe gDefault{|*|} mbval
		# mapAttr = 'Map'.fromList
			[("perspective", encodePerspective perspective)
			,("tilesUrls"
			 , JSONArray $
				(\tile ->
					JSONObject
						[ ("url", toJSON tile.url)
						: maybeToList $ (\attr -> ("attribution", toJSON $ toString attr)) <$> tile.attribution ]
				) <$>
					tilesUrls
			 )
			,(	"icons"
				, JSONArray
					[toJSON (iconId,{IconOptions|iconUrl=iconUrl,iconSize=[w,h]})
					\\ {iconId,iconUrl,iconSize=(w,h)} <- icons])
			,("viewMousePosition", toJSON viewMousePosition)
			,("viewMeasureDistance", toJSON viewMeasureDistance)
			,("viewLegend", toJSON (isJust viewLegend))
			:[("fitbounds", attr) \\ attr <- fitBoundsAttribute val]
			]
		# (markers, polylines, others) = partitionToEncodedMarkersPolylinesAndOthers objects state
		# attr = 'Map'.unions
			[ mapAttr, sizeAttr (ExactSize 500) (ExactSize 150)
			, 'Map'.singleton "markers" $ toJSON (True, markers)
			, 'Map'.singleton "polylines" $ toJSON polylines
			, attr]
		= (Ok (uiac UIHtmlView attr others, {currentOnClient = val, currentOnServer = val}, ?None), vst)

	// This partitions the encodings into markers, non-editable polylines and other objects.
	partitionToEncodedMarkersPolylinesAndOthers ::
		!(Map LeafletObjectID (LeafletObject s)) !s -> (![JSONNode], ![JSONNode], ![UI])
	partitionToEncodedMarkersPolylinesAndOthers objects state =
		// A foldr is used to make sure the markers remain order based on their id, as the id determines the z-index.
		'Map'.foldrWithKey` withObject ([], [], []) objects
	where
		withObject :: E.^ s:
			!LeafletObjectID !(LeafletObject s) !(![JSONNode], ![JSONNode], ![UI]) -> (![JSONNode], ![JSONNode], ![UI])
		withObject objectId object (markers, polylines, others) = case object of
			Marker m = ([encodeMarker objectId m state:markers], polylines, others)
			Polyline p | not p.LeafletPolyline.editable =
				(markers, [encodePolyline objectId p state: polylines], others)
			otherObj = (markers, polylines, [encodeUI objectId otherObj:others])

	encodePerspective :: !LeafletPerspective -> JSONNode
	encodePerspective (CenterAndZoom center zoom) = JSONArray
		[ JSONString "CenterAndZoom"
		, JSONArray [JSONReal center.lat, JSONReal center.lng]
		, JSONInt zoom
		]
	encodePerspective (FitToBounds options _) = JSONArray
		[ JSONString "FitToBounds"
		, toJSON options
		]

	encodeUI :: !LeafletObjectID !(LeafletObject s) -> UI
	encodeUI polylineId (Polyline o)
		# (JSONObject attr) = toJSON o
		= uia UIData ('Map'.fromList [("type",JSONString "polyline"), ("polylineId", toJSON polylineId):attr])
	encodeUI polygonId (Polygon o)
		# (JSONObject attr) = toJSON o
		= uia UIData ('Map'.fromList [("type",JSONString "polygon"), ("polygonId", toJSON polygonId): attr])
	encodeUI circleId (Circle o)
		# (JSONObject attr) = toJSON o
		= uia UIData ('Map'.fromList [("type",JSONString "circle"), ("circleId", toJSON circleId): attr])
	encodeUI rectangleId (Rectangle o)
		# (JSONObject attr) = toJSON o
		= uia UIData ('Map'.fromList [("type",JSONString "rectangle"), ("rectangleId", toJSON rectangleId): attr])
	encodeUI windowId (Window o)
		# (JSONObject attr) = toJSON o
		# dataMap = 'Map'.fromList [("type",JSONString "window"), ("windowId", toJSON windowId): attr]
		// translate HtmlTag to HTML code
		# dataMap` = 'Map'.put "content" (JSONString (toString o.content)) dataMap
		= uia UIData dataMap`

	initUI {FrontendEngineOptions|serverDirectory} me world
		# (jsInitDOM,world) = jsWrapFun (initDOM me) me world
		# (viewMousePos, world) = me .# "attributes.viewMousePosition" .? world
		# viewMousePos = jsValToBool viewMousePos == ?Just True
		# (viewMeasureDistance, world) = me .# "attributes.viewMeasureDistance" .? world
		# viewMeasureDistance = jsValToBool viewMeasureDistance == ?Just True
		# (viewLegend, world) = me .# "attributes.viewLegend" .? world
		# viewLegend = jsValToBool viewLegend == ?Just True
		# world = addCSSFromUrl (serverDirectory+++ITASKS_LEAFLET_CSS) ?None world
		# world = if viewMousePos (addCSSFromUrl (serverDirectory+++LEAFLET_MOUSEPOS_CSS) ?None world) world
		# world = if viewMeasureDistance (addCSSFromUrl (serverDirectory+++LEAFLET_DISTMEASURE_CSS) ?None world) world
		// We can only load the leaflet javascript if the leaflet css has already been loaded
		// hence we perform a callback on the add
		# (cb,world) =
			jsWrapFun (loadJS serverDirectory jsInitDOM viewMousePos viewMeasureDistance viewLegend me) me world
		# world = addCSSFromUrl (serverDirectory+++LEAFLET_CSS) (?Just cb) world
		= world
	loadJS serverDirectory jsInitDOM viewMousePos viewMeasureDistance viewLegend me _ world
		# world = addJSFromUrls
			([ (True, serverDirectory+++LEAFLET_JS)
			 , (True, serverDirectory+++LEAFLET_JS_EDITABLE)
			 , (True, serverDirectory+++PIXI_JS)
			 , (True, serverDirectory+++PIXI_OVERLAY_JS)
			 , (True, serverDirectory+++ITASKS_LEAFLET_JS)
			 , (True, serverDirectory+++LEAFLET_JS_MORPHDOM)
			 ] ++ map (tuple True) optionalUrls
			) (?Just jsInitDOM) me world
		= world
	where
		optionalUrls :: [String]
		optionalUrls
			=  if viewMousePos [serverDirectory +++ LEAFLET_MOUSEPOS_JS] [] ++
			   if viewMeasureDistance [serverDirectory +++ LEAFLET_DISTMEASURE_JS] []

	initDOM :: !JSVal !{!JSVal} !*JSWorld -> *JSWorld
	initDOM me args world
		# l = jsGlobal "L"
		# domEl = me .# "domEl"
		# jsRecordMapOptions =
			jsRecord
				[("viewOnly", toJS viewOnly), ("viewMousePosition", toJS viewMousePosition)
				,("viewMeasureDistance", toJS viewMeasureDistance), ("viewLegend", toJS viewLegend)]
		//Create the map; set `editable` option to `true` as shapes are possibly editable (causing exceptions otherwise)
		# (mapOpts, world)  = jsNew (jsGlobal "Object") (jsRecordMapOptions) world
		# world             = mapOpts .# "editable" .= True $ world
		# (mapObj,world)    = (l .# "map" .$ (domEl, mapOpts)) world
		# world             = (me .# "map" .= mapObj) world
		// Create global PIXI Container
		# (container, world) = jsNew "PIXI.Container" () world
		# world = me .# "container" .= container $ world
		# (overlay, world) = createPixiOverlay me mapObj world
		# world = me .# "overlay" .= overlay $ world
		//Set perspective
		# world             = setMapPerspective me True (me .# "attributes.perspective") world
		//Add icons
		# world             = setMapIcons me (me .# "attributes.icons") world
		//Create tile layer
		# tilesUrls = me .# "attributes.tilesUrls"
		# world             = forEach (addMapTilesLayer me mapObj) tilesUrls world
		//Synchronize lat/lng bounds to server (they depend on the size of the map in the browser)
		# world             = syncCurrentState me False world
		//Add initial objects
		# world = jsGlobal "leafletCreatePolylines" .$! me $ world
		# objects = me .# "children"
		# (windows, world) = jsNew "Map" () world
		# world            = (me .# "windows" .= windows) world
		# world            = (me .# "windowOffset" .= 0) world
		# world            = createMapObjects me mapObj objects world
		//Add mouse position control
		# (viewMousePos, world) = me .# "attributes.viewMousePosition" .? world
		# viewMousePos          = jsValToBool viewMousePos == ?Just True
		# world =
			if viewMousePos
				(snd $ l .# "control.mousePosition({formatter: L.dmsFormatter}).addTo" .$ mapObj $ world)
				world
		//Add distance measurement control
		# (viewMeasureDistance, world)  = me .# "attributes.viewMeasureDistance" .? world
		# viewMeasureDistance = jsValToBool viewMeasureDistance == ?Just True
		# unitControlTitleRecord = jsRecord
			[ "text" :> "Change units"
			, "metres" :> "metres" // No capitalization on purpose.
			, "landmiles" :> "land miles"
			, "nauticalmiles" :> "nautical miles"
			]
		# optionsRecord = jsRecord
			[ "measureControlTitleOn"   :> "Start measuring distances"
			, "measureControlTitleOff"  :> "Stop measuring distances"
			, "clearControlTitle"       :> "Clear measurements"
			, "unitControlTitle"        :> unitControlTitleRecord
			, "showClearControl"        :> True
			, "showUnitControl"         :> True
			, "clearMeasurementsOnStop" :> False
			, "measureControlLabel"     :> "&#8596;" // Right arrow icon for turning on/off measurement.
			, "unit"                    :> "nauticalmiles"
			, "backgroundColor"         :> "#00cc00"
			, "measureControlClasses"   :> ["leaflet-interactive"] // Makes cursor have same style as other buttons.
			, "clearControlClasses"     :> ["leaflet-interactive"]
			, "unitControlClasses"      :> ["leaflet-interactive"]
			]
		# world =
			if viewMeasureDistance
				(snd $ jsCall (l .# "control" .# "polylineMeasure") optionsRecord .# "addTo" .$ mapObj $ world)
				world
		//Add legend control
		# (viewLegend, world)  = me .# "attributes.viewLegend" .? world
		# viewLegend = jsValToBool viewLegend == ?Just True
		# optionsRecord = (jsRecord ["url" :> fromJust mapOptions.viewLegend, "label" :> "?"])
		# world = if viewLegend (snd $ jsCall (l .# "control" .# "legend") optionsRecord .# "addTo" .$ mapObj $ world) world
		//Add event handlers
		# (cb,world)       = jsWrapFun (\a w -> onResize me w) me world
		# world            = (me .# "onResize" .= cb) world
		# (cb,world)       = jsWrapFun (\a w -> onShow me w) me world
		# world            = (me .# "onShow" .= cb) world
		# (cb,world)       = jsWrapFun (\a w -> onAttributeChange me a w) me world
		# world            = (me .# "onAttributeChange" .= cb) world
		# (cb,world)       = jsWrapFun (\a w -> onAfterChildInsert me a w) me world
		# world            = (me .# "afterChildInsert" .= cb) world
		# (cb,world)       = jsWrapFun (\a w -> onBeforeChildRemove me a w) me world
		# world            = (me .# "beforeChildRemove" .= cb) world
		# (cb,world)       = jsWrapFun (\a w -> onViewportChange me w) me world
		# world            = (me .# "onViewportChange" .= cb) world
		# (vp,world)       = (me .# "getViewport" .$ ()) world
		# world            = (vp .# "addChangeListener" .$! me) world
		# (cb,world)       = jsWrapFun (\a w -> beforeRemove me w) me world
		# world            = (me .# "beforeRemove" .= cb) world
		# (cb,world)       = jsWrapFun (\a w -> onHtmlEvent me a w) me world
		# world            = (me .# "onHtmlEvent" .= cb) world
		# world = case viewOnly of
            True
				= world
			False
				# (cb,world)       = jsWrapFun (onMapMoveEnd me) me world
				# world            = (mapObj .# "addEventListener" .$! ("moveend",cb)) world
				# (cb,world)       = jsWrapFun (onMapMoveStart me) me world
				# world            = (mapObj .# "addEventListener" .$! ("movestart",cb)) world
				# (cb,world)       = jsWrapFun (onMapZoomStart me) me world
				# world            = (mapObj .# "addEventListener" .$! ("zoomstart",cb)) world
				# (cb,world)       = jsWrapFun (onMapZoomEnd me) me world
				# world            = (mapObj .# "addEventListener" .$! ("zoomend",cb)) world
				# (cb,world)       = jsWrapFun (onMapClick False me) me world
				# world            = (mapObj .# "addEventListener" .$! ("click",cb)) world
				# (cb,world)       = jsWrapFun (onMapClick True me) me world
				# world            = (mapObj .# "addEventListener" .$! ("dblclick",cb)) world
				= world
		# world = postInitUI mapObj world
		= world

	syncCurrentState me manualP world
		| viewOnly = world
		# mapObj            = me .# "map"
		# (bounds,world)    = getMapBounds mapObj world
		# (center,world)    = getMapCenter mapObj world
		# (zoom,world)      = getMapZoom mapObj world
		# edit = toJSON [LDSetBounds bounds,LDSetCenter center,LDSetZoom zoom: if manualP [LDSetManualPerspective] []]
		# world             = me .# "doEditEvent" .$! edit $ world
		= world

	onResize me world
		# world             = me .# "map.invalidateSize" .$! () $ world
		= world

	onShow me world
		# world             = me .# "map.invalidateSize" .$! () $ world
		# world             = setMapPerspective me True (me .# "attributes.perspective") world
		= world

	onMapMoveEnd :: !JSVal !{!JSVal} !*JSWorld -> *JSWorld
	onMapMoveEnd me args world
		# world = me .# "delayDrawMarkerMove" .= False $ world
		# (ignore,world) = me .# "attributes.ignoreNextOnmoveend" .?? (False, world)
		# world          = me .# "attributes.ignoreNextOnmoveend" .= False $ world
		# (delayDrawMarkerCb, world) = me .# "delayDrawMarkerCb" .? world
		# (zoomNotEnded,world) = me .# "delayDrawMarkerZoom" .?? (False, world)
		# world =
			if (jsIsNull delayDrawMarkerCb || jsIsUndefined delayDrawMarkerCb || zoomNotEnded)
				world (me .# "delayDrawMarkerCb" .= jsNull $ delayDrawMarkerCb .$! () $ world)
		| ignore = world
		= syncCurrentState me True world

	onMapMoveStart :: !JSVal !{!JSVal} !*JSWorld -> *JSWorld
	onMapMoveStart me args world
		# (tooltip, world) = jsDocument .# "getElementById" .$ "tooltip" $ world
		# world = me .# "delayDrawMarkerMove" .= True $ world
		| jsIsNull tooltip = world
		# world = (jsDocument .# "body.removeChild" .$! tooltip) world
		= world

	onMapZoomStart :: !JSVal !{!JSVal} !*JSWorld -> *JSWorld
	onMapZoomStart me args world = me .# "delayDrawMarkerZoom" .= True $ world

	onMapZoomEnd :: !JSVal !{!JSVal} !*JSWorld -> *JSWorld
	onMapZoomEnd me args world
		# world = me .# "delayDrawMarkerZoom" .= False $ world
		# (delayDrawMarkerCb, world) = me .# "delayDrawMarkerCb" .? world
		# (moveNotEnded,world) = me .# "delayDrawMarkerMove" .?? (False, world)
		= if (jsIsNull delayDrawMarkerCb || jsIsUndefined delayDrawMarkerCb || moveNotEnded)
			world (me .# "delayDrawMarkerCb" .= jsNull $ delayDrawMarkerCb .$! () $ world)

	onMapClick double me args world
		# clickPos  = args.[0] .# "latlng"
		# (position,world)  = toLatLng clickPos world
		# edit              = toJSON [if double LDMapDblClick LDMapClick position]
		# world             = (me .# "doEditEvent" .$! edit) world
		= world

	onMarkerClick :: !JSVal !LeafletObjectID !{!JSVal} !*JSWorld -> *JSWorld
	onMarkerClick me markerId args world
		# edit              = toJSON [LDMarkerClick markerId]
		# world             = (me .# "doEditEvent" .$! edit) world
		= world

	onAttributeChange :: !JSVal !{!JSVal} !*JSWorld -> *JSWorld
	onAttributeChange me args world
		// Map size should be invalidated as attribute which changes map size may have been changed.
		# world = (me .# "map.invalidateSize" .$! ()) world
		# data = args.[1]
		= case fromJS "" args.[0] of
			"perspective" = setMapPerspective me False data world
			"icons"       = setMapIcons me data world
			"fitbounds"   = fitBounds me False data (me .# "attributes.perspective" .# 1) world
			"markers"
				# (iconsChanged, world) = data .# "0" .?? (False, world)
				# world = me .# "attributes.markers" .= data $ world
				// If the icons changed, markers can only be rendered after the icons are updated.
				| iconsChanged = world
				= jsGlobal "leafletCreateMarkers" .$! me $ world
			"polylines"
				# world = me .# "attributes.polylines" .= data $ world
				= jsGlobal "leafletCreatePolylines" .$! me $ world
			"titles"
				= (jsGlobal "leafletCreateTitles" .$! (me, data)) world
			_ = jsTrace "unknown attribute change" world

	onHtmlEvent me args world
		= case (jsValToString args.[0]) of
			?Just event
				# edit = toJSON [LDHtmlEvent event]
				# world = me .# "doEditEvent" .$! edit $ world
				= world
			_	= world

	onAfterChildInsert me args world
		# l = jsGlobal "L"
		# mapObj    = me .# "map"
		= createMapObject me mapObj args.[1] l world

	// Called by itasks-core.js when a child of the leaflet editor is removed
	onBeforeChildRemove me args world
		# (type,world) = args.[1] .# "attributes.type" .? world
		= case jsValToString type of
			?Just "window" = removeWindow me args world
			?Just "marker" = jsTrace "This should not be reached, markers are no longer children" world
			?Just _ = removeOther  me args world
			_ = world
	where
		removeWindow me args world
			# (layer, world) = args.[1] .# "layer" .? world
			# windows = me .# "windows"
			// We must remove the window from the map before we continue, this makes sure we do not try
			// to access the window when the `markers` attribute is updated.
			# world = (layer .# "removeFromMap" .$! windows) world
			= removeOther me args world
		removeOther me args world
			# layer = args.[1] .# "layer"
			# world             = (me .# "map.removeLayer" .$! layer) world
			// for windows, based on control class
			# (removeMethod, world) = layer .# "remove" .? world
			| not (jsIsUndefined removeMethod) = (layer .# "remove" .$! ()) world
			// for all other objects
			= world

	onViewportChange me world
		# world             = (me .# "map.invalidateSize" .$! ()) world
		= world

	beforeRemove me world
		# (vp,world) = (me .# "getViewport" .$ ()) world
		# world      = (vp .# "removeChangeListener" .$! me) world
		# world      = (me .# "container" .# "destroy" .$! True) world
		# world      = (me .# "map" .# "remove" .$! ()) world
		= world

	// Called when the x button on a window is pressed
	onWindowRemove me windowId _ world
		// remove children from iTasks component
		# (children, world) = me .# "children" .? world
		# world             = forEach (removeWindow me) children world
		// send edit event to server
		# edit              = toJSON [LDRemoveWindow windowId]
		# world             = me .# "doEditEvent" .$! edit $ world
		= world
	where
		removeWindow me idx layer world
			# (layerWindowId, world) = layer .# "attributes.windowId" .?? ("", world)
			| LeafletObjectID layerWindowId == windowId
				/**
				* Callback which does nothing to prevent a race condition causing a JS error
				* stating that the callback of a child node is undefined.
				* See https://gitlab.com/clean-and-itasks/itasks-leaflet/-/issues/1.
				*/
				# world = me .# "children" .# idx .# "onUIChange" .= (jsGlobal "dummyCallback") $ world
				# windows = me .# "windows"
				# world = (layer .# "layer.removeFromMap" .$! windows) world
				= world
			= world

	//Map object access
	toLatLng obj world
		# (lat,world)     = obj .# "lat" .?? (0.0, world)
		# (lng,world)     = obj .# "lng" .?? (0.0, world)
		= ({LeafletLatLng|lat=lat,lng=lng}, world)

	toBounds bounds env
		# (sw,env)          = (bounds .# "getSouthWest" .$ ()) env
		# (ne,env)          = (bounds .# "getNorthEast" .$ ()) env
		# (swpos,env)       = toLatLng sw env
		# (nepos,env)       = toLatLng ne env
		= ({southWest=swpos,northEast=nepos},env)

	getMapBounds mapObj env
		# (bounds,env) = (mapObj .# "getBounds" .$ ()) env
		= toBounds bounds env

	getMapZoom mapObj world
		= (mapObj .# "getZoom" .$? ()) (1, world)

	getMapCenter mapObj world
		# (center,world) = (mapObj .# "getCenter" .$ ()) world
		= toLatLng center world

	setMapPerspective me initialization attr world
		# (type,world) = attr .# 0 .?? ("", world)
		= case type of
			"CenterAndZoom"
				# world = (me .# "map.setView" .$! (attr .# 1, attr .# 2)) world
				-> syncCurrentState me False world
			"FitToBounds"
				# world = fitBounds me initialization (me .# "attributes.fitbounds") (attr .# 1) world
				-> syncCurrentState me False world
			_
				-> jsTraceVal attr (jsTrace "failed to set perspective" world)

	fitBounds :: !JSVal !Bool !JSVal !JSVal !*JSWorld -> *JSWorld
	fitBounds me initialization bounds options world
		// Disable animations as they can cause issues with redrawing the markers
		// while the animation happens and the mouse is used to move the map at the same itme.
		# world = options .# "animate" .= False $ world
		// If we fit the bounds this will cause a `onmoveend` event, which has to be ignored.
		// Only when the map is initialized the event will not be fired.
		# world = if initialization world (me .# "attributes.ignoreNextOnmoveend" .= True $ world)
		# mapFitBoundsFuncName = if initialization "map.fitBounds" "map.flyToBounds"
		# world = (me .# mapFitBoundsFuncName .$! (bounds, options)) world
		= world

	addMapTilesLayer me mapObj _ tiles world
		# (tilesUrl, world)    = tiles .# "url" .? world
		| jsIsNull tilesUrl    = world
		# attribution          = tiles .# "attribution"
		# (options, world)     = jsEmptyObject world
		# world                = (options .# "attribution" .= attribution) world
		# l                    = jsGlobal "L"
		# (layer,world)        = l .# "tileLayer" .$ (tilesUrl, options) $ world
		# world                = layer .# "addTo" .$! mapObj $ world
		= world

	setMapIcons :: !JSVal !JSVal !*JSWorld -> *JSWorld
	setMapIcons me icons world
		# l                    = jsGlobal "L"
		# (iconIndex,world)    = jsEmptyObject world
		# world                = (me .# "icons" .= iconIndex) world
		# (textureIndex,world) = jsEmptyObject world
		# world                = (me .# "textures" .= textureIndex) world
		// use a loader to make sure initial markers are only drawn after all textures are loaded
		# (loader, world)      = jsNew "PIXI.Loader" () world
		# world                = forEach (createMapIcon l loader iconIndex textureIndex) icons world
		// load all textures and drawn initial markers when finished
		# (cb, world) = jsWrapFun (\_ -> jsGlobal "leafletCreateMarkers" .$! me) me world
		# world                = loader.# "load" .$! (cb) $ world
		= world
	where
		createMapIcon :: !JSVal !JSVal !JSVal !JSVal x !JSVal !*JSWorld -> *JSWorld
		createMapIcon l loader iconIndex textureIndex _ def world
			# (iconId,world)   = def .# 0 .?? ("", world)
			# iconSpec         = def .# 1
			# (icon,world)     = (l .# "icon" .$ iconSpec) world
			# world            = (iconIndex .# iconId .= icon) world
			# url              = icon .# "options.iconUrl"
			# (cb, world)      = jsWrapFun (onTextureLoaded iconId) me world
			# world            = loader .# "add" .$! (iconId, url, cb) $ world
			= world
		where
			onTextureLoaded :: !String !{!JSVal} !*JSWorld -> *JSWorld
			onTextureLoaded iconId args world
				# texture = args.[0] .# "texture"
				# world = textureIndex .# iconId .= jsRecord ["texture" :> texture] $ world
				= world

	createPixiOverlay :: !JSVal !JSVal !*JSWorld -> (!JSVal, !*JSWorld)
	createPixiOverlay me mapObj world
		# container            = me .# "container"
		# world                = (me .# "scale" .= 0.0) world
		# (cb, world)          = jsWrapFun drawCallback me world
		# (options, world)     = jsEmptyObject world
		# world                = (options .# "pane" .= "markerPane") world
		# (pixiOverlay, world) = (jsGlobal "L" .# "pixiOverlay" .$ (cb, container, options)) world
		# world                = (pixiOverlay .# "addTo" .$! mapObj) world
		= (pixiOverlay, world)
	where
		drawCallback :: !{!JSVal} !*JSWorld -> *JSWorld
		drawCallback args world
			# utils = args.[0]
			# (firstDraw, world) = me .# "firstDraw" .?? (True, world)
			# world = if firstDraw (init utils world) world
			= jsGlobal "leafletPixiDraw" .$! (me, utils) $ world
		where
			init :: !JSVal !*JSWorld -> *JSWorld
			init utils world
				# world = me .# "firstDraw" .= False $ world
				# container = me .# "container"
				# (renderer, world) = utils .# "getRenderer" .$ () $ world
				# interaction = renderer .# "plugins.interaction"
				// Required to make "on map click" events work on mobile devices.
				# world = interaction .# "autoPreventDefault" .= False $ world
				# (cb, world) = jsWrapFun (onPixiMapClick me container interaction) me world
				= (mapObj .# "on" .$! ("click", cb)) world

		onPixiMapClick :: !JSVal !JSVal !JSVal !{!JSVal} !*JSWorld -> *JSWorld
		onPixiMapClick me container interaction args world
			# jsEvent = args.[0]
			# pointerEvent = jsEvent .# "originalEvent"
			# (pixiPoint, world) = jsNew "PIXI.Point" () world
			# pex = pointerEvent .# "clientX"
			# pey = pointerEvent .# "clientY"
			# world = (interaction .# "mapPositionToPoint" .$! (pixiPoint, pex, pey)) world
			# (target, world) = (interaction .# "hitTest" .$ (pixiPoint, container)) world
			| jsIsNull target = world
			| otherwise
				# (markerId, world) = target .# "markerId" .?? ("", world)
				= onMarkerClick me (LeafletObjectID markerId) args world

	createMapObjects :: !JSVal !JSVal !JSVal !*JSWorld -> *JSWorld
	createMapObjects me mapObj objects world
		# l = jsGlobal "L"
		# world = forEach (\_ object world -> createMapObject me mapObj object l world) objects world
		= world

	createMapObject me mapObj object l world
		# (type,world) = object .# "attributes.type" .? world
		= case jsValToString type of
			?Just "polyline"  = createPolyline  me mapObj l object world
			?Just "polygon"   = createPolygon   me mapObj l object world
			?Just "circle"    = createCircle    me mapObj l object world
			?Just "rectangle" = createRectangle me mapObj l object world
			?Just "window"    = createWindow    me mapObj l object world
			?Just "marker"    = jsTrace "This code should not be reached, markers are not children" world
			_                 = world

	createPolyline :: !JSVal !JSVal !JSVal !JSVal !*JSWorld -> *JSWorld
	createPolyline me mapObj l object world
		//Set options
		# (options,world)     = jsEmptyObject world
		# style               = object .# "attributes.style"
		# world               = forEach (applyLineStyle options) style world
		# points              = object .# "attributes.points"
		# (layer,world)       = l .# "polyline" .$ (points ,options) $ world
		# world               = layer .# "addTo" .$! mapObj $ world
		# world               = enableEdit "polylineId" me layer object getUpdate world
		# world               = object .# "layer" .= layer $ world
		= world
	where
		getUpdate :: !JSVal !*JSWorld -> (!LeafletObjectUpdate, !*JSWorld)
		getUpdate layer world
			# (points, world) = (layer .# "getLatLngs" .$ ()) world
			# (points, world) = jsValToList` points id world
			# (points, world) = foldl (\(res, world) point = appFst (\latLng -> [latLng: res]) $ toLatLng point world)
			                          ([], world)
			                          points
			= (UpdatePolyline $ reverse points, world)

	createPolygon me mapObj l object world
		//Set options
		# (options,world)     = jsEmptyObject world
		# style               = object .# "attributes.style"
		# world               = forEach (applyAreaStyle options) style world
		# points              = object .# "attributes.points"
		# (layer,world)       = (l .# "polygon" .$ (points ,options)) world
		# world               = (layer .# "addTo" .$! mapObj) world
		# world               = enableEdit "polygonId" me layer object getUpdate world
		# world               = (object .# "layer" .= layer) world
		= world
	where
		getUpdate layer world
			# (points, world) = (layer .# "getLatLngs" .$ ()) world
			# points          = points .# 0
			# (points, world) = jsValToList` points id world
			# (points, world) = foldl (\(res, world) point = appFst (\latLng -> [latLng: res]) $ toLatLng point world)
			                          ([], world)
			                          points
			= (UpdatePolygon $ reverse points, world)

	createCircle me mapObj l object world
		//Set options
		# (options,world)     = jsEmptyObject world
		# style               = object .# "attributes.style"
		# world               = forEach (applyAreaStyle options) style world
		# center              = object .# "attributes.center"
		# radius              = object .# "attributes.radius"
		# world               = (options .# "radius" .= radius) world
		# (layer,world)       = (l .# "circle" .$ (center, options)) world
		# world               = (layer .# "addTo" .$! mapObj) world
		# world               = enableEdit "circleId" me layer object getUpdate world
		# world               = (object .# "layer" .= layer) world
		= world
	where
		getUpdate layer world
			# (radius,   world) = (layer .# "getRadius" .$? ()) (0.0, world)
			# (center,   world) = (layer .# "getLatLng" .$ ()) world
			# (center,   world) = toLatLng center world
			= (UpdateCircle center radius, world)

	createRectangle me mapObj l object world
		//Set options
		# (options,world)     = jsEmptyObject world
		# style               = object .# "attributes.style"
		# world               = forEach (applyAreaStyle options) style world
		# sw                  = object .# "attributes.bounds.southWest"
		# ne                  = object .# "attributes.bounds.northEast"
		# (layer,world)       = (l .# "rectangle" .$ ([sw, ne], options)) world
		# world               = (layer .# "addTo" .$! mapObj) world
		# world               = enableEdit "rectangleId" me layer object getUpdate world
		# world               = (object .# "layer" .= layer) world
		= world
	where
		getUpdate layer world
			# (bounds, world) = (layer .# "getBounds" .$ ()) world
			# (bounds, world) = toBounds bounds world
			= (UpdateRectangle bounds, world)

	enableEdit ::
		!String !JSVal !JSVal !JSVal !(JSVal *JSWorld -> (LeafletObjectUpdate, *JSWorld)) !*JSWorld -> *JSWorld
	enableEdit idFieldName me layer object getUpdate world
		# (isEditable,world)  = object .# "attributes.editable" .?? (False, world)
		| not isEditable = world
		# (_, world)  = (layer .# "enableEdit" .$ ()) world
		# (cb, world) = jsWrapFun (onEditing layer) me world
		// The `dragend` event is fired when existing points have been edited or new points have been added.
		# (_, world)  = (layer .# "addEventListener" .$ ("editable:vertex:dragend", cb)) world
		# (_, world)  = (layer .# "addEventListener" .$ ("editable:vertex:deleted", cb)) world
		= world
	where
		onEditing layer _ world
			# (update,   world) = getUpdate layer world
			# (objectId, world) = object .# "attributes." +++ idFieldName .?? ("", world)
			# edit              = toJSON [LDUpdateObject (LeafletObjectID objectId) update]
			# (_,        world) = me .# "doEditEvent" .$ edit $ world
			= world

	applyAreaStyle options _ style world
		# (styleType, world) = style .# 0 .? world
		# styleType = jsValToString styleType
		| styleType == ?Just "Style"
			# directStyle = style .# 1
			# (directStyleType, world) = directStyle .# 0 .? world
			# (directStyleVal, world)  = directStyle .# 1 .? world
			# directStyleType = jsValToString directStyleType
			= case directStyleType of
				?Just "AreaLineStrokeColor" = (options .# "color"       .= directStyleVal) world
				?Just "AreaLineStrokeWidth" = (options .# "weight"      .= directStyleVal) world
				?Just "AreaLineOpacity"     = (options .# "opacity"     .= directStyleVal) world
				?Just "AreaLineDashArray"   = (options .# "dashArray"   .= directStyleVal) world
				?Just "AreaNoFill"          = (options .# "fill"        .= False)          world
				?Just "AreaFillColor"       = (options .# "fillColor"   .= directStyleVal) world
				?Just "AreaFillOpacity"     = (options .# "fillOpacity" .= directStyleVal) world
				_                           = abort "unknown style"
		| styleType == ?Just "Class"
			# (cls, world) = style .# 1 .? world
			= (options .# "className" .= cls) world
		= abort "unknown style"

	calcInitPos initPos me world
		| jsIsUndefined initPos
			# (offset, world)    = me .# "windowOffset" .?? (0, world)
			# (initPos`, world)  = jsEmptyObject world
			# world              = (initPos` .# "x" .= fst DEFAULT_WINDOW_POS + offset) world
			# world              = (initPos` .# "y" .= snd DEFAULT_WINDOW_POS + offset) world
			# offset             = offset + 10
			# world              =
				(me .# "windowOffset" .= (if (offset > MAX_WINDOW_OFFSET) (offset - MAX_WINDOW_OFFSET) offset)) world
			= (initPos`, world)
		| otherwise = (initPos, world)

	createWindow :: !JSVal !JSVal !JSVal !JSVal !*JSWorld -> *JSWorld
	createWindow me mapObj l object world
		# (layer,world)      = l .# "window" .$ () $ world
		# world              = (object .# "layer" .= layer) world
		# (initPos, world)   = object .# "attributes.initPosition" .? world
		# (initPos,world)    = calcInitPos initPos me world
		# (_, world)         = (layer .# "setInitPos" .$ initPos) world
		# (morphDomCustom, world) = jsWrapFunWithResult morphDomForWindowElements me world
		# world              = (layer .# "morphDomForWindowElements" .= morphDomCustom) world
		# title              = object .# "attributes.title"
		# (_, world)         = (layer .# "setTitle" .$ title) world
		# (_, world)         = layer .# "setContent" .$ (object .# "attributes.content") $ world
		# relMarkers = object .# "attributes.relatedMarkers"
		# world              = forEach (addRelatedMarker layer) relMarkers world
		// inject function to send event on window remove
		# world = case viewOnly of
			True
				= world
			False
				# (windowId,world)   = object .# "attributes.windowId" .?? ("", world)
				# (onWRemove, world) = jsWrapFun (onWindowRemove me (LeafletObjectID windowId)) me world
				= (layer .# "_onWindowClose" .= onWRemove) world
		// inject function to handle window update
		# (cb,world)         = jsWrapFun (onUIChange layer) me world
		# world              = ((object .# "onUIChange") .= cb) world
		// add to map
		# world              = (layer .# "addTo" .$! mapObj) world
		# children = me .# "container.children"
		# world              = (layer .# "onCreate" .$! children) world
		= world
	where
		addRelatedMarker :: !JSVal idc !JSVal !*JSWorld -> *JSWorld
		addRelatedMarker layer _ relMarker world
			# world = (layer .# "addRelatedMarker" .$! relMarker) world
			# markerId = relMarker .# 0
			# (windows, world) = (me .# "windows.get" .$ markerId) world
			| jsIsUndefined windows = createNewSet me markerId layer world
			| otherwise = (windows .# "add" .$! layer) world


		// If `JSVal True` is returned, the element is updated, if `JSVal False` is returned the value is not updated.
		// See: https://github.com/patrick-steele-idem/morphdom?tab=readme-ov-file#morphdomfromnode-tonode-options--node
		morphDomForWindowElements :: !{!JSVal} !*JSWorld -> (!JSVal, !*JSWorld)
		morphDomForWindowElements args world
			| size args <> 2 = abort "keepSelectedAttrCategoryTheSame called without 2 arguments."
			# fromEl = args.[0]
			# toEl = args.[1]
			// This uses an optimization which is described here:
			// https://github.com/patrick-steele-idem/morphdom?tab=readme-ov-file#can-i-make-morphdom-blaze-through-the-dom-tree-even-faster-yes
			// There are minor concerns related to performing this optimization, which aren't described in detail.
			// For the use cases within real-world iTasks applications, so far no issues have been encountered.
			# (equal, world) = fromEl .# "isEqualNode" .$? toEl $ (False, world)
			| equal = (toJS False, world)
			| otherwise = (toJS True, morphChangedWindowElements fromEl toEl world)

		onUIChange :: !JSVal !{!JSVal} !*JSWorld -> *JSWorld
		onUIChange layer changes world
			# world = foldl doChange world [c \\ c <-: changes]
			= world
		where
			doChange :: !*JSWorld !JSVal -> *JSWorld
			doChange world change
				# attrUpdates = change .# "attributes"
				# world = forEach updateAttr attrUpdates world
				= world

			updateAttr :: idc !JSVal !*JSWorld -> *JSWorld
			updateAttr _ attrChange world
				# (name,  world) = attrChange .# "name" .?? ("", world)
				# value = attrChange .# "value"
				= case name of
					"content"        = layer .# "setContent"        .$! value $ world
					"title"          = layer .# "setTitle"          .$! value $ world
					"relatedMarkers" = updateRelatedMarkers me value world
					_                = abort $ concat ["unknown attribute of leaflet window: \"", name, "\"\n"]
			where
				updateRelatedMarkers :: !JSVal !JSVal !*JSWorld -> *JSWorld
				updateRelatedMarkers me value world
					# world = layer .# "setRelatedMarkers" .$! (me, value) $ world
					# world = forEach (\_ relMarker world
						# world = (layer .# "addRelatedMarker" .$! relMarker) world
						# markerId = relMarker .# 0
						# (windows, world) = (me .# "windows.get" .$ markerId) world
						| jsIsUndefined windows = createNewSet me markerId layer world
						| otherwise = (windows .# "add" .$! layer) world
						) value world
					= world

	createNewSet :: !JSVal !JSVal !JSVal !*JSWorld -> *JSWorld
	createNewSet me markerId layer world
		# (set, world) = jsNew "Set" () world
		# world = (set .# "add" .$! layer) world
		# world = (me .# "windows.set" .$! (markerId, set)) world
		= world

	applyLineStyle :: !JSVal idc !JSVal !*JSWorld -> *JSWorld
	applyLineStyle options _ style world
	# (styleType, world) = style .# 0 .? world
	# styleType = jsValToString styleType
	| styleType == ?Just "Style"
		# directStyle = style .# 1
		# (directStyleType, world) = directStyle .# 0 .? world
		# (directStyleVal, world)  = directStyle .# 1 .? world
		# directStyleType = jsValToString directStyleType
		= case directStyleType of
			?Just "LineStrokeColor" = (options .# "color"     .= directStyleVal) world
			?Just "LineStrokeWidth" = (options .# "weight"    .= directStyleVal) world
			?Just "LineOpacity"     = (options .# "opacity"   .= directStyleVal) world
			?Just "LineDashArray"   = (options .# "dashArray" .= directStyleVal) world
			_                       = abort "unknown style"
	| styleType == ?Just "Class"
		# cls = style .# 1
		= (options .# "className" .= cls) world
	= abort "unknown style"

	//Process the edits received from the client
	onEdit ::
		![LeafletEdit] !(InternalState s) !*VSt
		-> (!*MaybeError String *(UIChange, (InternalState s), ?(LeafletMap s)), !*VSt)
	onEdit diffs st=:{currentOnServer} vst
		# (currentOnServer, currentOnClient) = foldl withDiff (currentOnServer, currentOnServer) diffs
		# st & currentOnServer = currentOnServer, currentOnClient = currentOnClient
		= (Ok (NoChange, st, ?Just currentOnServer), vst)
	where
		// The updates (e.g. perspective changes) are already done on the client except for remove window.
		// So the client is set to a state where the non-remove window changes have already been taken into account.
		// For remove window, the window is already removed from the map layer on the client, but not from the children
		// within the container.
		withDiff :: !(!LeafletMap s, !LeafletMap s) !LeafletEdit -> (!LeafletMap s, !LeafletMap s)
		withDiff (currentOnServerNew, currentOnClient) diff
			| diff =: LDRemoveWindow _ = (app currentOnServerNew diff, currentOnClient)
			= (app currentOnServerNew diff, app currentOnClient diff)

		app :: !(LeafletMap s) LeafletEdit -> LeafletMap s
		app m LDSetManualPerspective      = case (m.LeafletMap.center, m.zoom) of
			(?Just center, ?Just zoom) = {m & perspective=CenterAndZoom center zoom}
			_                          = m // should not happen
		app m (LDSetZoom zoom)            = {LeafletMap|m & zoom = ?Just zoom}
		app m (LDSetCenter center)        = {LeafletMap|m & center = ?Just center}
		app m (LDSetBounds bounds)        = {LeafletMap|m & bounds = ?Just bounds}
		app m (LDRemoveWindow idToRemove) =
			{LeafletMap|m & objects = 'Map'.del idToRemove m.LeafletMap.objects}
		app m (LDClosePopup markerId)
			= {LeafletMap|m & objects = 'Map'.alter withClosedPopup markerId m.LeafletMap.objects}
		where
			withClosedPopup :: !(?(LeafletObject s)) -> ?(LeafletObject s)
			withClosedPopup (?Just (Marker m)) = ?Just (Marker {m & popup = ?None})
			withClosedPopup _ = ?None
		app m (LDUpdateObject objectId upd) =
			{LeafletMap|m & objects = 'Map'.alter (fmap withUpdatedObject) objectId m.LeafletMap.objects}
		where
			withUpdatedObject :: !(LeafletObject s) -> LeafletObject s
			withUpdatedObject obj = case (obj, upd) of
				(Polyline polyline, UpdatePolyline points)
					= Polyline {LeafletPolyline| polyline & points = points}
				(Polygon polygon, UpdatePolygon points)
					= Polygon {LeafletPolygon| polygon & points = points}
				(Circle circle, UpdateCircle center radius)
					= Circle {LeafletCircle| circle & center = center, radius = radius}
				(Rectangle rect, UpdateRectangle bounds)
					= Rectangle {LeafletRectangle| rect & bounds = bounds}
				_ = obj
		app m (LDMarkerHover objectId) = {m & markersHoveredOver = 'Set'.insert objectId m.markersHoveredOver}
		app m (LDCancelMarkerHover objectId) = {m & markersHoveredOver = 'Set'.delete objectId m.markersHoveredOver}
		app m _ = m

	//Check for changed objects and update the client
	onRefresh ::
		E.^s: !(?(LeafletMap s)) !(InternalState s) !*VSt
		-> (!*MaybeError String *(UIChange, (InternalState s), ?(LeafletMap s)), !*VSt)
		| JSONEncode{|*|}, gEq{|*|}, gDefault{|*|} s
	onRefresh mbNewMap {currentOnClient} vst
		# newMap=:{LeafletMap|state, markersHoveredOver} = fromMaybe gDefault{|*|} mbNewMap
		//Determine attribute changes
		# (iconsChanged, attrChanges) = diffAttributes currentOnClient newMap
		//Separate markers from other objects
		# (oldMarkers, oldPolylines, oldOthers) =
			separateMarkersAndPolylinesFromOtherObjects currentOnClient.LeafletMap.objects
		# (newMarkers, newPolylines, newOthers) = separateMarkersAndPolylinesFromOtherObjects newMap.LeafletMap.objects
		//Determine object changes
		# childChanges =
			diffChildren oldOthers newOthers updateFromOldToNew (\(objectId, object) = encodeUI objectId object)
		# attrChanges = if (isEmpty childChanges)
			attrChanges
			([SetAttribute "fitbounds" attr \\ attr <- fitBoundsAttribute newMap] ++ attrChanges)
		# encodedNewMarkers = toEncodedMarkers iconsChanged newMarkers state
		# encodedNewPolylines = toEncodedPolylines newPolylines state
		# encodedNewTitles = toEncodedTitles newMarkers markersHoveredOver state
		# attrChanges =
			if (encodedNewTitles <> JSONArray [])
				[SetAttribute "titles" encodedNewTitles:attrChanges] attrChanges
		# attrChanges =
			if (maybe (oldMarkers =!= newMarkers) (\pred = pred currentOnClient newMap) sendMarkersToClientPred)
				[SetAttribute "markers" $ encodedNewMarkers: attrChanges] attrChanges
		# attrChanges = if (oldPolylines =!= newPolylines)
			[SetAttribute "polylines" $ encodedNewPolylines: attrChanges] attrChanges
		= (Ok (ChangeUI attrChanges childChanges, {currentOnClient = newMap, currentOnServer = newMap}, ?None),vst)
	where
		// The client needs to know whether the icons changed for updating the markers.
		toEncodedMarkers :: !Bool ![(LeafletObjectID, LeafletMarker s)] !s -> JSONNode
		toEncodedMarkers iconsChanged newMarkers state =
			toJSON (iconsChanged, (\(id, m) = encodeMarker id m state) <$> newMarkers)

		toEncodedPolylines :: ![(LeafletObjectID, LeafletPolyline)] !s -> JSONNode
		toEncodedPolylines newPolylines state = toJSON $ (\(id, p) = encodePolyline id p state) <$> newPolylines

		toEncodedTitles :: ![(LeafletObjectID, LeafletMarker s)] !(Set LeafletObjectID) !s -> JSONNode
		toEncodedTitles newMarkers markersHoveredOver state =
			toJSON $
				[encodeTitle id m.LeafletMarker.title state \\ (id, m) <- newMarkers
				| 'Set'.member id markersHoveredOver
				]

		// This partitions the objects into markers, non-editable polylines and other objects.
		separateMarkersAndPolylinesFromOtherObjects ::
			!(Map LeafletObjectID (LeafletObject s)) ->
			(![(LeafletObjectID, LeafletMarker s)], ![(LeafletObjectID, LeafletPolyline)]
			, ![(LeafletObjectID, LeafletObject s)])
		// A foldr is used to make sure the markers remain order based on their id, as the id determines the z-index.
		separateMarkersAndPolylinesFromOtherObjects objects = 'Map'.foldrWithKey` partitionFunc ([], [], []) objects
		where
			partitionFunc ::
				!LeafletObjectID !(LeafletObject s)
				!(![(LeafletObjectID, LeafletMarker s)], ![(LeafletObjectID, LeafletPolyline)]
				, ![(LeafletObjectID, LeafletObject s)])
				->
				( ![(LeafletObjectID, LeafletMarker s)], ![(LeafletObjectID, LeafletPolyline)]
				, ![(LeafletObjectID, LeafletObject s)])
			partitionFunc objectId object (markerAcc, polylineAcc, othersAcc) = case object of
				Marker m = ([(objectId, m): markerAcc], polylineAcc, othersAcc)
				Polyline p=:{LeafletPolyline| editable} | not editable =
					(markerAcc, [(objectId, p): polylineAcc], othersAcc)
				_ = (markerAcc, polylineAcc, [(objectId, object): othersAcc])

		//Only center and zoom are synced to the client, bounds are only synced from client to server
		diffAttributes :: !(LeafletMap s) !(LeafletMap s) -> (!Bool, ![UIAttributeChange])
		diffAttributes {LeafletMap|perspective=p1,icons=i1} n=:{LeafletMap|perspective=p2,icons=i2}
			//Perspective
			# perspective = if (p1 === p2) []
				[SetAttribute "perspective" (encodePerspective p2)
				: [SetAttribute "fitbounds" attr \\ attr <- fitBoundsAttribute n]]
			//Icons
			# icons =
				if (i2 === i1)
					[]
					[SetAttribute "icons"
						(JSONArray
							[toJSON (iconId,{IconOptions|iconUrl=iconUrl,iconSize=[w,h]})
							\\ {LeafletIcon|iconId,iconUrl,iconSize=(w,h)} <- i2])
					]
			= (not $ isEmpty icons, perspective ++ icons)

		updateFromOldToNew ::
			!(!LeafletObjectID, !LeafletObject s) !(!LeafletObjectID, !LeafletObject s) -> ChildUpdate | gEq{|*|}, JSONEncode{|*|} s
		updateFromOldToNew (oldWindowId, Window old) (newWindowId, Window new)
			| oldWindowId === newWindowId && not (isEmpty changes) = ChildUpdate $ ChangeUI changes []
		where
			changes = catMaybes
				[ if (old.LeafletWindow.title == new.LeafletWindow.title)
				     ?None
				     (?Just $ SetAttribute "title" $ toJSON $ new.LeafletWindow.title)
				, if (old.content === new.content)
				     ?None
				     (?Just $ SetAttribute "content" $ toJSON $ toString new.content)
				, if (old.relatedMarkers === new.relatedMarkers)
				     ?None
				     (?Just $ SetAttribute "relatedMarkers" $ toJSON new.relatedMarkers)
				]

		updateFromOldToNew old new | old === new = NoChildUpdateRequired
		                           | otherwise   = ChildUpdateImpossible

	writeValue :: !(InternalState s) -> MaybeError idc (LeafletMap s)
	writeValue {currentOnServer} = Ok currentOnServer

	fitBoundsAttribute :: !(LeafletMap s) -> [JSONNode]
	fitBoundsAttribute {perspective=p=:FitToBounds _ region,objects} = [encodeBounds (bounds region)]
	where
		bounds :: !FitToBoundsRegion -> LeafletBounds
		bounds (SpecificRegion bounds) = bounds
		bounds (SelectedObjects ids) =
			leafletBoundingRectangleOf $
			'DF'.foldl`
				(\acc objectId = maybe acc (\obj = 'Map'.put objectId obj acc) $ 'Map'.get objectId objects)
				'Map'.newMap ids
		bounds AllObjects = leafletBoundingRectangleOf objects

		encodeBounds :: !LeafletBounds -> JSONNode
		encodeBounds {southWest=sw,northEast=ne} = JSONArray
			[ JSONArray [JSONReal sw.lat, JSONReal sw.lng]
			, JSONArray [JSONReal ne.lat, JSONReal ne.lng]
			]
	fitBoundsAttribute _ = []


gEditor{|LeafletMapWithoutState|} purpose =
	mapEditorWrite ValidEditor $ leafEditorToEditor $
	leafEditorForLeafletMap {defaultMapOptions & viewOnly = purpose =: ViewValue} (const id)

gDefault{|LeafletMap|} s =
	{ LeafletMap
	| perspective        = defaultValue
	, bounds             = ?None
	, center             = ?None
	, zoom               = ?None
	, tilesUrls          = [openStreetMapTiles]
	, objects            = 'Map'.singleton (LeafletObjectID "home") (Marker homeMarker)
	, icons              = [blackSquareIcon]
	, state              = s
	, markersHoveredOver = 'Set'.newSet
	}
where
	homeMarker =
		{ tint = ?None
		, rotation = ?None
		, position = {LeafletLatLng|lat = 51.82, lng = 5.86}
		, title = \_ = ?Just $ Text "HOME"
		, icon = ?Just blackSquareID
		, popup = ?None
		}

	blackSquareID :: LeafletIconID
	blackSquareID = LeafletIconID "black_square"

	blackSquareIcon :: LeafletIcon
	blackSquareIcon =
		{ LeafletIcon
		| iconId = blackSquareID
		, iconUrl = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKEAYAAADdohP+AAABcGlDQ1BpY2MAACiRdZE7SwNBFIW/RCU+IiKKiFikiGJhQBREO41FmiAhKhi1STYvIZssuwkSbAUbC8FCtPFV+A+0FWwVBEERRCz8Bb4aCesdE0iQZJbZ+3FmzmXmDDiDGU23msdAz+bNcMDvWY6seFxvtNFHO91MRzXLmA2FgjQc3w84VL33qV6N99UdHfGEpYGjVXhSM8y88IxwcCNvKN4R7tXS0bjwsfCoKQcUvlF6rMyvilNl/lRsLobnwKl6elI1HKthLW3qwiPCXj1T0CrnUTdxJ7JLC1IHZA5iESaAHw8xCqyTIY9PalYyq+8b+/PNkxOPJn+DIqY4UqTFOypqQbompCZFT8iXoahy/5+nlZwYL3d3+6HlxbY/hsC1B6Vd2/45se3SKTQ9w1W26s9JTlNfou9WNe8RdG3BxXVVi+3D5Tb0PxlRM/onNcl0JpPwfg6dEei5g/bVclaVdc4eYXFTnugWDg5hWPZ3rf0CAfloC2Ri6QAAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAA1SURBVCgVY2QAg///ITTlJAvMiP9gAOORTjOCAQMDE+la8esYNRB/+BAjO/jDkBHiDeolbADKMAoRMpO23QAAAABJRU5ErkJggg=="
		, iconSize = (24, 24)
		}

gDefault{|LeafletPerspective|} = CenterAndZoom {LeafletLatLng|lat = 51.82, lng = 5.86} 7

gDefault{|LeafletBounds|} =
	{ southWest = {lat=50.82, lng=4.86}
	, northEast = {lat=52.82, lng=6.86}
	}

gEq{|LeafletMarker|} gEqS marker0 marker1 =
	// Do not check for equality of title as it is a function taking a state.
	marker0.position === marker1.position && marker0.rotation === marker1.rotation && marker0.tint === marker1.tint &&
	marker0.LeafletMarker.icon === marker1.LeafletMarker.icon && marker0.popup == marker1.popup

//Comparing reals may have unexpected results, especially when comparing constants to previously stored ones
gEq{|LeafletLatLng|} x y = (toString x.lat == toString y.lat) && (toString x.lng == toString y.lng)

simpleStateEventHandlers :: LeafletEventHandlers LeafletSimpleState
simpleStateEventHandlers =
	[ OnMapClick \position map=:{LeafletMap|state} =
		 {LeafletMap|addCursorMarker position map & state.cursor = ?Just position}
	, OnMarkerClick \markerId map=:{LeafletMap|state}
		= {LeafletMap|map & state.selection = toggle markerId state.LeafletSimpleState.selection}
	]
where
	addCursorMarker :: !LeafletLatLng !(LeafletMap s) -> LeafletMap s
	addCursorMarker position l=:{LeafletMap|objects,icons} =
		{l & objects = addCursorObjectForMostRecentlyClickedPosition objects, icons=addCursorIcon icons}
	where
		addCursorObjectForMostRecentlyClickedPosition ::
			!(Map LeafletObjectID (LeafletObject s)) -> Map LeafletObjectID (LeafletObject s)
		addCursorObjectForMostRecentlyClickedPosition objects =
			'Map'.put (LeafletObjectID "cursor") (cursor position) $
				'Map'.filterWithKey
					(\objectId object = not (objectId == LeafletObjectID "cursor" && object=:(Marker _))) objects
		addCursorIcon [] = [icon]
		addCursorIcon [i=:{iconId}:is]
			| iconId =: (LeafletIconID "cursor") = [i:is]
			| otherwise = [i:addCursorIcon is]

	cursor position = Marker
		{LeafletMarker|position= position
		, rotation = ?None,tint = ?None,icon = ?Just (LeafletIconID "cursor"),title = \_ = ?None,popup = ?None}
	icon =
		{ LeafletIcon|iconId=LeafletIconID "cursor", iconUrl= svgIconURL (CircleElt hattrs sattrs) (10,10)
		, iconSize = (10,10)}
	where
        sattrs = [CxAttr (SVGLength "5" PX),CyAttr (SVGLength "5" PX),RAttr (SVGLength "3" PX)]
		hattrs = [StyleAttr "fill:none;stroke:#00f;stroke-width:2"]

	toggle :: !LeafletObjectID ![LeafletObjectID] -> [LeafletObjectID]
	toggle (LeafletObjectID "cursor") xs = xs //The cursor can't be selected
	toggle x xs = if (isMember x xs) (removeMember x xs) ([x:xs])

customLeafletEditor ::
	!(MapOptions s) !(LeafletEventHandlers s) s -> Editor (LeafletMap s) (LeafletMap s)
	| JSONEncode{|*|}, gEq{|*|}, gDefault{|*|}, TC s
customLeafletEditor mapOptions handlers initial =
	leafEditorToEditor (customLeafEditorForLeafletMap mapOptions handlers initial)

customLeafEditorForLeafletMap ::
	!(MapOptions s) !(LeafletEventHandlers s) s ->
	LeafEditor [LeafletEdit] (InternalState s) (LeafletMap s) (LeafletMap s)
	| gEq{|*|}, JSONEncode{|*|}, gDefault{|*|}, TC s
customLeafEditorForLeafletMap mapOptions handlers initial =
	{ LeafEditor
	| onReset        = onReset
	, onEdit         = onEdit
	, onRefresh      = onRefresh
	, writeValue     = writeValue
	}
where
	baseEditor = leafEditorForLeafletMap mapOptions $ case [h \\ OnMapDblClick h <- handlers] of
		[_:_] -> \me -> me .# "doubleClickZoom" .# "disable" .$! ()
		[]    -> const id

	onReset ::
		E.^s: !UIAttributes !(?(LeafletMap s)) !*VSt
		-> (!*MaybeError String (UI, InternalState s, ?(LeafletMap s)), !*VSt)
	onReset attributes mbMap vst = baseEditor.LeafEditor.onReset attributes mbMap vst

	onEdit ::
		E.^ s: ![LeafletEdit] !(InternalState s) !*VSt
		-> (!*MaybeError String *(UIChange, (InternalState s), ?(LeafletMap s)), !*VSt)
	onEdit edits mapState vst
		# (res, vst) = baseEditor.LeafEditor.onEdit edits mapState vst
		= case res of
			Ok (change, st=:{currentOnServer}, _)
				// Leave `currentOnClient` unchanged, as the updates to the custom state are not reflected on the
				// client yet.
				# currentOnServer = updateCustomState handlers edits currentOnServer
				= (Ok (change, {st & currentOnServer = currentOnServer}, ?Just currentOnServer), vst)
			Error e = (Error e, vst)

	onRefresh ::
		E.^ s: !(?(LeafletMap s)) !(InternalState s) !*VSt
		-> (!*MaybeError String (UIChange, InternalState s, ?(LeafletMap s)), !*VSt)
	onRefresh mbNewMap oldMap vst = baseEditor.LeafEditor.onRefresh mbNewMap oldMap vst

	writeValue :: !(InternalState s) -> MaybeError e (LeafletMap s)
	writeValue {currentOnServer} = Ok currentOnServer

	updateCustomState :: !(LeafletEventHandlers s) ![LeafletEdit] !(LeafletMap s) -> LeafletMap s
	updateCustomState handlers edits leafletMap = foldl (\s e -> foldl (update e) s handlers) leafletMap edits
	where
		update (LDMapClick position)           leafletMap (OnMapClick f)         = f position leafletMap
		update (LDMapDblClick position)        leafletMap (OnMapDblClick f)      = f position leafletMap
		update (LDMarkerClick markerId)        leafletMap (OnMarkerClick f)      = f markerId leafletMap
		update (LDHtmlEvent event)             leafletMap (OnHtmlEvent f)        = f event leafletMap
		update (LDMarkerHover markerId)        leafletMap (OnMarkerHover f)      = f markerId leafletMap
		update (LDCancelMarkerHover markerId)  leafletMap (OnMarkerHoverLeave f) = f markerId leafletMap
		update _                               leafletMap _                      = leafletMap

instance == LeafletObjectID where (==) (LeafletObjectID x) (LeafletObjectID y) = x == y
instance == LeafletIconID where (==) (LeafletIconID x) (LeafletIconID y) = x == y

instance < LeafletObjectID where (<) (LeafletObjectID x) (LeafletObjectID y) = x < y

gDefault{|FitToBoundsOptions|} =
	{ padding = (100, 100)
	, maxZoom = 10
	}

derive JSONEncode LeafletMap, LeafletLatLng, TileLayer, LeafletMarker
derive JSONDecode LeafletMap, LeafletLatLng, TileLayer, LeafletMarker
derive gDefault   LeafletLatLng, LeafletSimpleState, LeafletObjectID
derive gEq        LeafletMap, TileLayer
derive gText      LeafletMap, LeafletLatLng, TileLayer, LeafletMarker
derive gEditor    LeafletLatLng, LeafletMarker
derive class iTask
	LeafletIcon, LeafletBounds, LeafletObject, LeafletPolyline,
	LeafletPolygon, LeafletEdit, LeafletWindow, LeafletWindowPos,
	LeafletLineStyle, LeafletStyleDef, LeafletAreaStyle, LeafletObjectID,
	CSSClass, LeafletIconID, LeafletCircle, LeafletObjectUpdate,
	LeafletRectangle, LeafletSimpleState, LeafletPerspective,
	FitToBoundsOptions, FitToBoundsRegion

defaultMapOptions :: MapOptions a
defaultMapOptions =
	{ viewOnly = False
	, viewMousePosition = False
	, viewMeasureDistance = False
	, viewLegend = ?None
	, attributionControl = True
	, zoomControl = True
	, morphChangedWindowElements = \_ _ world = world
	, sendMarkersToClientPred = ?None
	}

encodeMarker :: !LeafletObjectID !(LeafletMarker s) !s -> JSONNode
encodeMarker objectId {position, rotation, tint, title, icon, popup} state =
	JSONArray
		[ toJSON objectId
		, JSONObject $ catMaybes
			[?Just ("position", toJSON position), (\rotation = ("rotation", toJSON rotation)) <$> rotation
			, (\tint = ("tint", toJSON tint)) <$> tint
			, (\icon = ("icon", toJSON $ icon)) <$> icon , (\popup = ("popup", toJSON $ toString popup)) <$> popup]]

encodePolyline :: !LeafletObjectID !LeafletPolyline !s -> JSONNode
encodePolyline objectId p state = JSONObject [("id", toJSON objectId), ("attributes", toJSON p)]

encodeTitle :: !LeafletObjectID !(s -> ?HtmlTag) !s -> JSONNode
encodeTitle objectId stateToTitle state =
	JSONArray [toJSON objectId, maybe JSONNull toJSON mbTitleHtml]
where
	mbTitleHtml = toString <$> stateToTitle state

instance == LeafletLatLng derive gEq

derive genShow LeafletCircle, LeafletLatLng, LeafletAreaStyle, LeafletStyleDef, CSSClass
derive gPrint LeafletCircle, LeafletLatLng, LeafletAreaStyle, LeafletStyleDef, CSSClass
derive ggen LeafletCircle, LeafletLatLng, LeafletAreaStyle, LeafletStyleDef, CSSClass
