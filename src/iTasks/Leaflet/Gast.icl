implementation module iTasks.Leaflet.Gast

import Control.GenBimap
import Gast.Gen
import Gast.GenLibTest
import StdEnv

from iTasks.Leaflet import :: LeafletBounds, :: LeafletLatLng

derive ggen LeafletBounds, LeafletLatLng
derive genShow LeafletBounds, LeafletLatLng
