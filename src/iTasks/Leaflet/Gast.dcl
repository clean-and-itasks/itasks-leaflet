definition module iTasks.Leaflet.Gast

from Gast.Gen import generic ggen, :: GenState
from Gast.GenLibTest import generic genShow

from iTasks.Leaflet import :: LeafletBounds, :: LeafletLatLng

derive ggen LeafletBounds, LeafletLatLng
derive genShow LeafletBounds, LeafletLatLng
