implementation module iTasks.Leaflet.GenPrint

import Text.GenPrint

import iTasks.Leaflet

derive gPrint LeafletBounds, LeafletLatLng
