definition module iTasks.Leaflet.GenPrint

from Text.GenPrint import generic gPrint, class PrintOutput, :: PrintState

from iTasks.Leaflet import :: LeafletBounds, :: LeafletLatLng

derive gPrint LeafletBounds, LeafletLatLng
