definition module iTasks.Leaflet

/**
 * @property-bootstrap
 *      import Data.Real
 *      import StdEnv
 */

from ABC.Interpreter.JavaScript import :: JSWorld, :: JSVal
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Data.Map import :: Map
from Data.Set import :: Set
from Gast.Gen import generic ggen, :: GenState
from Gast.GenLibTest import generic genShow
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.UI.Editor.Generic import generic gEditor, :: EditorPurpose, :: EditorReport, :: Editor
from iTasks.WF.Definition import class iTask
from StdClass import class <
from Text.GenJSON import generic JSONDecode, generic JSONEncode, :: JSONNode
from Text.GenPrint import generic gPrint, :: PrintState, class PrintOutput
from Text.HTML import :: HtmlTag, :: SVGElt

/**
* Simple leaflet map without state.
*
* @param Whether the editor is created for viewing
* @result An editor which is used to edit the leaflet map.
*/
leafletEditor :: !Bool -> Editor LeafletMapWithoutState LeafletMapWithoutState

/*
 * Allows to customize the leaflet map for more complex use cases.
 *
 * @param `MapOptions` for the leaflet map.
 * @param Handlers for events.
 * @param Initial value of the state of the leaflet map.
 * @result Editor which is used to edit the leaflet map.
 */
customLeafletEditor ::
	!(MapOptions s) !(LeafletEventHandlers s) s -> Editor (LeafletMap s)  (LeafletMap s)
	| JSONEncode{|*|}, gEq{|*|}, gDefault{|*|}, TC s

/**
 * Produces a generic leaflet editor for a leaflet editor with state provided the class instances match for the state.
 * This can be used to implement `gEditor` for leaflet maps with custom states.
 *
 * @param Whether the editor is used for viewing or altering a leaflet map.
 * @result The editor, which matches the generic editor for a leaflet map without state..
 */
leafletEditorGeneric ::
	!EditorPurpose -> Editor (LeafletMap s) (EditorReport (LeafletMap s))
	| gEq{|*|}, gDefault{|*|}, JSONEncode{|*|}, TC s

/**
 * This record being extended is not considered a breaking change.
 * Note that this type may be extended at any point in time, therefore it is recommended to not create records of
 * this type directly, but to use the record update syntax in combination with `defaultMapOptions` instead.
 * As in: {defaultMapOptions & viewOnly = True, ...}.
 */
:: MapOptions s =
	{ viewOnly                   :: !Bool //* Whether the map is view-only.
	, viewMousePosition          :: !Bool //* Whether to show the coordinates for the mouse position.
	, viewMeasureDistance        :: !Bool //* Whether to show the controls for measuring distance.
	, viewLegend                 :: !?String //* Maybe url to webpage showing legend.
	, attributionControl         :: !Bool //* Whether the attribute control is shown.
	, zoomControl                :: !Bool //* Whether the zoom control is shown.
	, morphChangedWindowElements :: !JSVal JSVal *JSWorld -> *JSWorld
		//* Function which morphs the DOM elements which will change within an open leaflet window.
		//* The first argument is the old state of the element
		//* The second argument becomes the new state of the element after applying the function.
		//* The point of this function is that the second argument can be altered
		//* based on the first element, resulting in a new element.
	, sendMarkersToClientPred    :: !?((LeafletMap s) (LeafletMap s) -> Bool)
	 	//* If provided, this is a custom equality predicate for seeing if markers should be sent to the client
		//* The first argument is the old map, the second argument is the new map.
		//* In case this is false, no data is sent to the client.
		//* If `?None` it is checked whether the markers are the same using generic equality.
	}

//* The default map options.
defaultMapOptions :: MapOptions a

//* Type representing a leaflet GIS map based on https://leafletjs.com/.
:: LeafletMap s =
	{ perspective        :: !LeafletPerspective //* How the map should decide what should be in view
	, bounds             :: !?LeafletBounds //* The actual bounds of the map (updated by the client; writes are ignored)
	, center             :: !?LeafletLatLng //* The actual center of the map (updated by the client; writes are ignored)
	, zoom               :: !?Int //* The actual zoom level of the map (updated by the client; writes are ignored)
	, tilesUrls          :: ![TileLayer] //* The URLs serving the tiles
	, objects            :: !Map LeafletObjectID (LeafletObject s)
		//* Map with identifiers for objects to the respective objects shown in the map.
		//* E.g: markers, lines and polygons.
	, icons              :: ![LeafletIcon]
		//* Custom icons used by markers. They are referenced using their 'iconId' string.
	, state              :: !s //* State of the leaflet map, if applicable.
	, markersHoveredOver :: !Set LeafletObjectID //* The leaflet markers that are being hovered over.
	}

//* A leaflet map with an empty state, used to define a generic editor for a leaflet map.
:: LeafletMapWithoutState :== LeafletMap ()

/**
 * This type describes how the application prefers the map to be drawn.
 *
 * When the perspective is `FitToBounds`, a view is automatically computed such
 * that a given region or set of objects is visible.
 *
 * `CenterAndZoom` simply sets the center coordinate and zoom level.
 *
 * When the user drags or zooms the map, the perspective of a `LeafletMap` is
 * reset to `CenterAndZoom`. This prevents applications from automatically
 * moving the view after the user has changed it. It is of course possible to
 * programmatically change it back to `FitToBounds`.
 */
:: LeafletPerspective
	= CenterAndZoom !LeafletLatLng !Int
	| FitToBounds !FitToBoundsOptions !FitToBoundsRegion

:: FitToBoundsOptions =
	{ padding  :: !(!Int, !Int) //* The horizontal and vertical padding in pixels
	, maxZoom  :: !Int          //* The maximum zoom level
	}

:: FitToBoundsRegion
	= SpecificRegion !LeafletBounds          //* Fits the view to the specified bounds.
	| SelectedObjects !(Set LeafletObjectID) //* Fits the view s.t. the given object IDs are visible.
	| AllObjects                             //* Fits the view s.t. all `objects` are visible.

:: TileLayer = {url :: !String, attribution :: !?HtmlTag}

:: LeafletIconID =: LeafletIconID String
:: LeafletIcon =
	{ iconId                     :: !LeafletIconID
	, iconUrl                    :: !String
	, iconSize                   :: !(!Int,!Int)
	}

:: LeafletLatLng =
	{ lat :: !Real
	, lng :: !Real
	}

:: LeafletBounds =
	{ southWest :: !LeafletLatLng
	, northEast :: !LeafletLatLng
	}

//* Objects that can be drawn over the underlying map.
:: LeafletObject s
	= Marker    !(LeafletMarker s) //* An image which is drawn on the map at a certain position.
	| Polyline  !LeafletPolyline //* A line consisting of multiple points which are connected.
	| Polygon   !LeafletPolygon
		//* A structure created through connecting multiple points and filling the space between the points.
	| Circle    !LeafletCircle //* A circle which is drawn over the map.
	| Rectangle !LeafletRectangle //* A rectangle which is drawn over the map.
	| Window    !LeafletWindow //* A popup window which is drawn on top of the map.

/**
 * Returns the points which capture bounds of the provided leaflet object.
 * E.g for a rectangle the points of all the corners of the rectangle are returned.
 * For circles, the top-, bottom-, left- and rightmost points are returned.
 *
 * @param The object to retrieve the points which capture the bounds of the object for.
 * @result The points which capture the bounds of the object.
 * @property center of circle should only be included if it's radius is 0: A.circle :: LeafletCircle:
 *    isMember circle.LeafletCircle.center (leafletPointsOf (Circle circle)) <==>
 *    approximatelyEqual 1.0 circle.radius zero
 * @precondition -90.0 <= circle.LeafletCircle.center.lat && circle.LeafletCircle.center.lat <= 90.0
 * @precondition -180.0 <= circle.LeafletCircle.center.lng && circle.LeafletCircle.center.lng <= 180.0
 */
leafletPointsOf :: !(LeafletObject s) -> [LeafletLatLng]
leafletBoundingRectangleOf :: !(Map LeafletObjectID (LeafletObject s)) -> LeafletBounds

:: LeafletObjectID =: LeafletObjectID String

:: LeafletMarker s =
	{ position      :: !LeafletLatLng //* The position for the which the marker is drawn.
	, rotation      :: !?Int //* The rotation of the marker in degrees.
	, tint          :: !?String //* Tint which is used to color the icon, can be a hex color code such as "0xffffff".
	, title         :: !s -> ?HtmlTag //* This is the tooltip shown when the marker is hovered over.
	, icon          :: !?LeafletIconID //* Reference to an icon defined for this map.
	, popup         :: !?HtmlTag //* Popup HTML which is always shown for the marker.
	}

:: LeafletPolyline =
	{ points        :: ![LeafletLatLng]
	, style         :: ![LeafletStyleDef LeafletLineStyle]
	, editable      :: !Bool
	}

:: LeafletPolygon =
	{ points        :: ![LeafletLatLng]
	, style         :: ![LeafletStyleDef LeafletAreaStyle]
	, editable      :: !Bool
	}

:: LeafletCircle =
	{ center   :: !LeafletLatLng
	, radius   :: !Real            //* the radius (in meters)
	, style    :: ![LeafletStyleDef LeafletAreaStyle]
	, editable :: !Bool
	}

derive genShow LeafletCircle
derive gPrint LeafletCircle
derive ggen LeafletCircle

:: LeafletRectangle =
	{ bounds        :: !LeafletBounds
	, style         :: ![LeafletStyleDef LeafletAreaStyle]
	, editable      :: !Bool
	}

:: LeafletWindow =
	{ initPosition   :: !?LeafletWindowPos
	, title          :: !String
	, content        :: !HtmlTag
	, relatedMarkers :: ![(LeafletObjectID, [LeafletStyleDef LeafletLineStyle])]
		//* connecting lines are drawn between the window and the markers to visualise the relation
	}

:: LeafletLineStyle
	= LineStrokeColor !String // html/css color definition
	| LineStrokeWidth !Int
	| LineOpacity     !Real   // between 0.0 and 1.0
	| LineDashArray   !String // a list of comma separated lengths of alternating dashes and gaps (e.g. "1,5,2,5")

:: LeafletAreaStyle
	= AreaLineStrokeColor !String // html/css color definition
	| AreaLineStrokeWidth !Int
	| AreaLineOpacity     !Real   // between 0.0 and 1.0
	| AreaLineDashArray   !String // a list of comma separated lengths of alternating dashes and gaps (e.g. "1,5,2,5")
	| AreaNoFill                  // inside of polygone is not filled, all other fill options are ignored
	| AreaFillColor       !String // html/css color definition
	| AreaFillOpacity     !Real

:: CSSClass =: CSSClass String
:: LeafletStyleDef style = Style style | Class CSSClass

:: LeafletWindowPos = { x :: !Int, y :: !Int }

//Event handlers allow the customization of the map editor behaviour
:: LeafletEventHandlers s :== [LeafletEventHandler s]

:: LeafletEventHandler s
	= OnMapClick         !(LeafletLatLng (LeafletMap s) -> LeafletMap s)
		//* Event which is triggered when the map is clicked once.
		//* Function receives the click position and the map at the time of clicking.
	| OnMapDblClick      !(LeafletLatLng (LeafletMap s) -> LeafletMap s)
		//* Event which is triggered when the map is double clicked.
		//* Function receives the click position and the map at the time of clicking.
	| OnMarkerClick      !(LeafletObjectID (LeafletMap s) -> LeafletMap s)
		//* Event which is triggered when a marker is clicked.
		//* Function receives the id of the marker which is clicked and the map at the time of clicking.
	| OnHtmlEvent        !(String (LeafletMap s) -> LeafletMap s)
		//* Event which is triggered in relation to an iTasks html event (see `toHtmlEventCall`).
		//* Function receives the name of the event provided to `iTasks.htmlEvent` and the map.
	| OnMarkerHover      !(LeafletObjectID (LeafletMap s) -> LeafletMap s)
		//* Event which is triggered when the marker is hovered over.
		//* Function receives the id of the marker which is hovered over and the map.
	| OnMarkerHoverLeave !(LeafletObjectID (LeafletMap s) -> LeafletMap s)
		//* Event which is triggered when the marker is no longer hovered over.
		//* Function receives the id of the marker which is no longer hovered over and the map.

//* A minimal state for tracking a set of selected markers and the last place that the map was clicked.
:: LeafletSimpleState =
	{ cursor    :: ?LeafletLatLng
	, selection :: [LeafletObjectID]
	}

simpleStateEventHandlers :: LeafletEventHandlers LeafletSimpleState

//Inline SVG based icons can be encoded as 'data uri's' which can be used instead of a url to an external icon image
svgIconURL :: !SVGElt !(!Int,!Int) -> String

//Public tileserver of openstreetmaps
openStreetMapTiles :: TileLayer

instance == LeafletObjectID
instance == LeafletIconID
instance == LeafletLatLng

instance < LeafletObjectID

derive JSONEncode LeafletMap, LeafletLatLng
derive JSONDecode LeafletMap, LeafletLatLng
derive gDefault   LeafletMap, LeafletLatLng, LeafletObjectID
derive gEq        LeafletMap, LeafletLatLng
derive gText      LeafletMap, LeafletLatLng
derive gEditor    LeafletMapWithoutState, LeafletLatLng
derive class iTask
	LeafletIcon, LeafletBounds, LeafletObject, LeafletPolyline,
	LeafletPolygon, LeafletWindow, LeafletWindowPos, LeafletLineStyle,
	LeafletStyleDef, LeafletAreaStyle, LeafletObjectID, CSSClass,
	LeafletIconID, LeafletCircle, LeafletRectangle, LeafletSimpleState,
	LeafletPerspective, FitToBoundsOptions, FitToBoundsRegion
