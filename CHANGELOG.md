#### 6.0.13

- Enhancement: speed up checking difference between Leaflet Windows through morphdom optimization.

#### 6.0.12

- Enhancement: add genShow, gPrint derivation for `:: LeafletBounds` and `:: LeafletLatLng`.

#### 6.0.11

- Enhancement: add ggen derivation for `:: LeafletBounds` and `:: LeafletLatLng`.

#### 6.0.10

- Chore: adapt to iTasks 0.16.0.

#### 6.0.9

- Fix: prevent multiple `onClick` events generated when clicking on the map.

#### 6.0.8

- Fix: fix z-order of leaflet tooltips and leaflet windows by downgrading pixi v7 to v6.
       since pixi v7, it would be possible for markers to be hovered over when a leaflet window was above the marker.

#### 6.0.7

- Fix: prevent issues with markers of editable objects not being draggable.

#### 6.0.6

- Chore: accept `base` `3.0`.

#### 6.0.5

- Fix: layout of window titlebar if content has less width than title.

#### 6.0.4

- Fix: updating leaflet maps after edit events for the ordinary `leafletEditor`. (`customLeafEditorForLeafletMap` was
       not affected.)

#### 6.0.3

- Fix: sync bounds back to server if using `CenterAndZoom` to change perspective.

#### 6.0.2

- Chore: support `iTasks 0.15`.

#### 6.0.1

- Fix: removing a window would not trigger a RemoveChild event. This would trigger ui change events to be triggered
       for the wrong child of the leaflet map for leaflet windows as the children of the leaflet map
       would not be updated after removing a window from the map.

## 6.0.0

- Feature: Set cursor to pointer when hovering over sprites (and back to grab afterwards)
- Removed: iconHitRectScalingFactor for `LeafletIcon`.
- Fix: prevent superfluous update from client while point added to polygon is still dragged to the first position.

#### 5.0.21

- Fix: do not move map to make new popups visible, as this can cause crashes in rare timing circumstances. (Zooming
       animations have been source of issues in the past already, see e.g. `3.1.1`.)

#### 5.0.20

- Fix: minor fix for window updates (from `morphdom` changelog: fix morphing duplicate ids of incompatible tags).
- Chore: adapt to `iTasks 0.14`.

#### 5.0.19

- Enhancement: do not longer add `#close` or `#collapse` to url when the
               close/collapse button of a leaflet window is clicked.
- Fix: Properly set height of window to its full height when the leaflet window is uncollapsed.
       This fixes the borders of leaflet windows not being shown when a leaflet window is
	   collapsed and then uncollapsed afterwards.

#### 5.0.18

- Fix: include `node_modules` directory in published package.

#### 5.0.17

- Chore: adapt to containers ^2.

#### 5.0.16

- Chore: support iTasks 0.13 alongside iTasks 0.12.

#### 5.0.15

- Chore: update various node packages.

#### 5.0.14

- Chore: Support iTasks 0.12.0 (to support base 2.0).

#### 5.0.13

- Enhancement: optimise drawing (non-editable) polylines.
- Enhancement: optimise applying changes of editable objects to the server model.

#### 5.0.12

- Chore: update node dependencies.

#### 5.0.11

- Chore: update iTassk dependency: accept iTasks ^0.11.0.

#### 5.0.10

- Fix: prevent superfluous updates sent back to the client on edits reflected on the client already (e.g. perspective
       changes).

#### 5.0.9

- Chore: update iTassk dependency: accept iTasks ^0.10.0.

#### 5.0.8

- Fix: marker id processing order, the ordering on a `LeafletObjectId` should
       determine the z-index of the marker but it would not, as the markers
       were sent to the client in reverse order.

#### 5.0.7

- Chore: update nitrile/devcontainer dependency.

#### 5.0.6

- Fix: crashes when initialising map caused by race conditions.
- Fix: crashes when updating icons after initialisation.
- Chore: update node modules.

#### 5.0.5

- Chore: adapt to iTasks 0.8.

#### 5.0.4

- Fix: calculating GIS bounds of an area would only take into account the center of the area
       , causing leaflet to zoom in too far and the bounds of the circular area to not be visible
	   when adjusting the perspective of the leaflet map to fit the bounds of the map objects.

#### 5.0.3

- Fix: Handling of multiple edit events before the editor was refreshed.

#### 5.0.2

- Fix: Pass on correct state for the old map for `onEdit` handler for `customLeafletEditor`.

#### 5.0.1

- Fix: prevent JS error being caused by race condition when removing a window
       inbetween the time it takes the server
       to recalculate the new map.
- Fix: Polylines related to a window are now always removed when closing the window
       to which the polylines are related.

## 5.0.0

- Change: remove option to perform an animation when flying to a location
          as it could cause markers to be drawn outside the scope
          of a region when moving the map while the animation was in progress.

#### 4.0.4

- Fix: call marker draw callback whenever `flyTo` finishes to avoid not drawing markers.

#### 4.0.3

- Enhancement: Pass JS values by reference instead of copying when possible.

#### 4.0.2

- Enhancement: Remove redundant `onRefresh` call in the `onEdit` handler of the `customLeafletEditor`.

#### 4.0.1

- Enhancement: Reduce memory usage on the client's Clean heap by not converting the content of windows to Clean strings.

## 4.0.0

- Change: Allow scaling the hitbox for mouse interaction of leaflet icons seperately for each icon instead of
	providing one scaling factor for all of the icons of the map.
- Change: gDefault instance for `:: MapOptions s` and add `defaultMapOptions` instead.
- Fix: wait for `flyTo` animation to end before calling `setZoom` to avoid a timing problem.

#### 3.1.3 (Broken)

- Fix: Flying to a certain location on the map now makes sure the zoom end event is triggered even though the
	zoom was interrupted.

#### 3.1.2

- Fix: the bounds of the map are now correctly calculated
	when the perspective is set to fit the bounds of selected objects.

#### 3.1.1

- Enhancement: do not draw markers while the user is zooming/moving the map.

### 3.1.0

- Feature: Add `markerHitRectScalingFactor` map option.

#### 3.0.1

- Enhancement: Delay sending marker mouse hover events to prevent too many events if the user moves the mouse over a
               large number of markers.

## 3.0.0

- Change, add `:: LeafletTitle` with `LazyTitle` constructor
	which allows to lazily send the title of a leaflet marker to
	the client based on a function which returns `?HtmlTag` given
	the state of the leafletmap.
## 2.0.0

- Change: Change type of `:: LeafletMap` `objects` field from `[LeafletObject]` to `Map LeafletObjectId LeafletObject`.
- Change: Remove id fields from all leaflet objects.

#### 1.0.2

- Fix: removing loading of root directory as JS if `viewMousePosition`
	of `LeafletMap` was enabled.  This would cause a console error.

#### 1.0.1

- Fix: Include node module configuration, required for the iTask web resource collector to include JS/CSS files.

## 1.0.0
- Feature: iTasks.Leaflet:
	Allow specifying a custom function which can be used to morph (alter)
	the elements of a leaflet window when
	the state of the window changes.
- Initial version, contains the leaflet extension which used to be directly
	part of the iTasks-SDK in the past.
